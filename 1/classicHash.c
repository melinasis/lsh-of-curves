#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "prog.h"
#include "grid.h"
#include "hashTable.h"

int keyValue(nodePtr g, int max, int numCurves, int ar_ri[]){
  //εσωτερικο γινομενο: αθροισμα rixi
  int a, b, i, num, xi, ri, x = 0, tableSize;
  long long sum = 0, M;

  tableSize = numCurves / 2;
  // printf("tableSize %d\n", tableSize);
  M = pow(2, 32) - 5;
  // printf("M %lld\n", M);

  num = countNode(g);
  // printf("NUMBER OF NODES: %d\n", num);

  for(i = 0; i < num; i++){
    xi = (int)g->point;
    ri = ar_ri[i];
    // printf("%d * %d\n",xi,ri );

    a = (xi*ri) % M;
    sum += a;

    g = g->next;
  }
  sum = sum % tableSize;
  if(sum < 0) sum = -sum;
  return sum;
}
void classicHash(nodePtr g, int max, int numCurves, BucketPtr* HashTable, int id, int ar_ri[]){
  int key = keyValue(g, max, numCurves, ar_ri);
  // printf("KEY is %d\n", key);
  insertHashTable(HashTable, key, id, g);
}

BucketPtr* createHashTable(int numCurves){

  BucketPtr* HashTable = malloc(((numCurves)/2)*sizeof(BucketPtr));
  if(HashTable == NULL)
  {
        printf("Error! memory not allocated.");
        EXIT_FAILURE;
  }
  int i;
  for(i = 0; i < (numCurves)/2; i++){
    HashTable[i] = malloc(sizeof(BucketList));
    if(HashTable[i] == NULL)
    {
          printf("Error! memory not allocated.");
          EXIT_FAILURE;
    }
    HashTable[i]->id = -1;
    HashTable[i]->g = NULL;
    HashTable[i]->next = NULL;
  }
  return HashTable;
}

void insertHashTable(BucketPtr* HashTable, int key, int id, nodePtr g){
  BucketPtr current = HashTable[key];
  if(current->id == -1){    //first node
    current->id = id;
    current->g = g;
  }
  else{
    while(current->next != NULL){
      current = current->next;
    }
    current->next = malloc(sizeof(BucketList));
    if(current->next == NULL)
    {
          printf("Error! memory not allocated.");
          EXIT_FAILURE;
    }
    current->next->id = id;
    current->next->g = g;
    current->next->next = NULL;
  }

}

void printHashTable(BucketPtr* HashTable, int numCurves){
  int i;
  for(i = 0; i < numCurves / 2; i++){   //HashTable cells
    printf("BUCKET %d:    ", i);
    BucketPtr current = HashTable[i];
    if(current->id != -1){
      printf("%d  ", current->id);
    }
      while(current->next != NULL){
      current = current->next;
      if(current->id != -1){
        printf("%d  ", current->id);
      }
    }
    printf("\n");
  }
}


void freeBucketList(BucketPtr head, int l){
  BucketPtr temp;
  BucketPtr current = head;
  while(current != NULL){
    if(l == 0){
      freeGridList(current->g);
    }
    temp = current;
    current = current->next;
    free(temp);
  }
}

void freeHashTable(BucketPtr* HashTable, int numCurves, int l){
  int i;
  for(i = 0; i < numCurves/2; i++){
    freeBucketList(HashTable[i], l);
  }
  free(HashTable);
}

int maxBuck(BucketPtr head){
  int maxBucket = 0;
  BucketPtr current = head;
  if(current->id != -1){
    while(current != NULL){
      maxBucket++;
      current = current->next;
    }
  }
  return maxBucket;
}
