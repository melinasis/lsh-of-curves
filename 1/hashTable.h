#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

typedef struct Neighbor* Neigh;
typedef struct Neighbor{
	int id;
	int n_id;
	long double dist;
}Neighbor;

typedef struct BucketList* BucketPtr;
typedef struct node* nodePtr; 						//pointer to node

typedef struct BucketList{
	int id;
	nodePtr g;
  BucketPtr next;
}BucketList;

BucketPtr* createHashTable(int numCurves);
void insertHashTable(BucketPtr* HashTable, int key, int id,  nodePtr g);
void printHashTable(BucketPtr* HashTable, int numCurves);
void freeHashTable(BucketPtr* HashTable, int numCurves, int l);
void freeBucketList(BucketPtr head, int l);

//classic Hash
void classicHash(nodePtr g, int max, int numCurves, BucketPtr* HashTable, int id, int ar_ri[]);
int keyValue(nodePtr g, int max, int numCurves, int ar_ri[]);

//LSH
void LSH_Hash(nodePtr g, int numCurves, BucketPtr* LSH_HashTable, int id, int ar_ri[]);
double t_calc(int w);
void push_h(nodePtr head, long double p);
void concatenate_lsh(nodePtr head, nodePtr newList);
int LSH_keyValue(nodePtr g, int numCurves, int ar_ri[]);
void free_amplified(nodePtr head);

//query
void query(curvePtr* curveAr, BucketPtr** LSH_Ar, long double delta, int max, int numCurves, int ar_ri[], char* queryFile, int K, int L, int f, FILE *outf);
int search(BucketPtr* HashTable, int key, nodePtr query_g, int numCurves, curvePtr* curveAr, curvePtr queryCurve, Neigh* neighR, Neigh neigh, int f, double R);
void searchAllBucket(BucketPtr* HashTable, int key, nodePtr query_g, int numCurves, curvePtr* curveAr, curvePtr queryCurve, Neigh* neighR, Neigh neigh, int f, double R);
void searchTrue(BucketPtr* HashTable, nodePtr query_g, int numCurves, curvePtr* curveAr, curvePtr queryCurve, Neigh neigh, int f);
long double DFD_function(curvePtr p,curvePtr q);
long double DTW_function(curvePtr p,curvePtr q);
long double max(long double d1,long double d2);
long double min(long double d1,long double d2,long double d3);
long double dist(long double *x,long double *y);
int maxBuck(BucketPtr head);
