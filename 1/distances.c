#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "prog.h"
#include "grid.h"
#include "hashTable.h"

long double max(long double d1,long double d2){
  if (d1>d2){
    return d1;
  }else{
    return d2;
  }
}

long double min(long double d1,long double d2,long double d3){
  if(d2<=d1 && d2<=d3 ){
    return d2;
  }else if(d1<=d2 && d1<=d3){
    return d1;
  }else if(d3<=d1 && d3<=d2){
    return d3;
  }
}

long double dist(long double *x,long double *y){
  int i;
  long double sqr,power,diff,sum=0;
  for(i=0;i<D;i++){
    //printf("x=%Lf,y=%Lf\n",x[i],y[i]);
    diff=x[i]-y[i];
    //printf("diff=%Lf\n",diff);
    power=pow(diff,2);
    //printf("pow=%Lf\n",power);
    sum+=sum+power;
  }
  //printf(" final=%Lf\n",sqr);
  sqr=sqrt(sum);
  //printf(" final=%Lf\n",sqr);
  return sqr;
}

long double DFD_function(curvePtr p,curvePtr q)
{

  long double maxd, mind ,d;
  int m = q->points;
  int n = p->points;

  long double L_ar[n][m];
  int i,j;

  for(i=0;i<n;i++){
    for(j=0;j<m;j++){
      if(i==0 && j==0){

        L_ar[i][j]=dist(p->coordinates,q->coordinates);
        //printf("0,0=%Lf\n",L_ar[i][j]);
      }else if (i==0){
        d=dist(p->coordinates,q->coordinates);
        maxd=max(d,L_ar[i][j-1]);
        L_ar[i][j]=maxd;
        //printf("0,%d=%Lf\n",j,L_ar[i][j]);
      }else if(j==0){
        d=dist(p->coordinates,q->coordinates);
        maxd=max(d,L_ar[i-1][j]);
        L_ar[i][j]=maxd;
        //printf("%d,0=%Lf\n",i,L_ar[i][j]);
      }else{
        d=dist(p->coordinates,q->coordinates);
        mind=min(L_ar[i-1][j],L_ar[i][j-1],L_ar[i-1][j-1]);
        maxd=max(d,mind);
        L_ar[i][j]=maxd;
        //printf("%d,%d=%Lf\n",i,j,L_ar[i][j]);
      }
    }
  }
  //printf("final table=%Lf\n",L_ar[n-1][m-1]);
  return L_ar[n-1][m-1];
}

long double DTW_function(curvePtr p,curvePtr q){
  int i,j,m1,m2;
  long double mind,d;
  m1=p->points;
  m2=q->points;
  long double C_ar[m1][m2];
  for(i=0;i<m1;i++){
    C_ar[i][0]=1029;
    //printf(" %d,0=%Lf\n",i,C_ar[i][0]);
  }
  for(i=0;i<m2;i++){
    C_ar[0][i]=1209;
    //printf(" 0,%d=%Lf\n",i,C_ar[0][j]);
  }
  C_ar[0][0]=0;
  //printf(" 0,0=%Lf\n",C_ar[i][j]);
  for(i=1;i<m1;i++){
    for(j=1;j<m2;j++){
      mind=min(C_ar[i-1][j],C_ar[i][j-1],C_ar[i-1][j-1]);
      d=dist(p->coordinates,q->coordinates);
      C_ar[i][j]=d+mind;
      //printf("%d,%d=%Lf\n",i,j,C_ar[i][j]);
    }
  }
  return C_ar[m1-1][m2-1];
}
