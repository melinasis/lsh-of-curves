#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "prog.h"
#include "grid.h"
#include "hashTable.h"

int main(int argc, char *argv[]) {
  int st;
  Pinfo pinfo = malloc(sizeof(Prog));
  if(pinfo == NULL)
  {
        printf("Error! memory not allocated.");
        EXIT_FAILURE;
  }
  memset(pinfo->hash, '\0', sizeof(pinfo->hash));
  memset(pinfo->input,'\0',sizeof(pinfo->input));
  memset(pinfo->query,'\0',sizeof(pinfo->query));
  memset(pinfo->output,'\0',sizeof(pinfo->query));

  FILE *outf;

  if (argHandling(pinfo, argc, argv) == EXIT_FAILURE){
  		printHelp(argv[0]);
  		return EXIT_FAILURE;
  }

  outf = fopen(pinfo->output,"w");  //output file

  int K = pinfo->K;
  int L = pinfo->L;

  fprintf(outf,"K %d \n",pinfo->K );
  fprintf(outf,"L %d\n",pinfo->L );
  srand (time(NULL));
  clock_t begin = clock();

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int i, j, k, points, temp1, temp2, flag, numCurves, hash, l;
  int duplicates = 0, min = 0,  max = 0, position = 0;
  long double x, y , delta,temp;
  nodePtr g;
  BucketPtr* Classic_HashTable;
  BucketPtr** LSH_Ar;


  stream = fopen(pinfo->input, "r" );     //first read of input file
  while ((read = getline(&line, &len, stream)) != -1) {
    line[read - 1] = 0;
    char *pch;

    pch = strtok(line, "\t");
    numCurves = atoi(pch) + 1;  //id starts with 0

    pch = strtok(NULL, "\t");
    pch[strlen(pch)] = 0;
    points = atoi(pch);
    // fprintf(outf,"points %d\n", points);
    if(min == 0){
      min = points;
    }
    else{
      if(points < min)  min = points;
    }

    if(points > max)  max = points;
  }
  fprintf(outf,"Curves are %d\n", numCurves);
  delta = (4*D*min);
  fprintf(outf,"max is %d\n", max);
  fprintf(outf,"DELTA = %Lf\n", delta);

  fclose(stream);

  curvePtr* curveAr = malloc(numCurves*sizeof(curvePtr));
  if(curveAr == NULL)
  {
        printf("Error! memory not allocated.");
        EXIT_FAILURE;
  }

  stream = fopen(pinfo->input, "r" );     //second read of input file

  LSH_Ar = malloc(L * sizeof(BucketPtr*));
  if(LSH_Ar == NULL)
  {
        printf("Error! memory not allocated.");
        EXIT_FAILURE;
  }
  if(pinfo->h == 1){
    fprintf(outf,"HashFunction: Classic\n");
    for(l = 0; l < L; l++){
      LSH_Ar[l] = createHashTable(numCurves);
    }
  }
  else if(pinfo->h == 2){
    fprintf(outf,"HashFunction: Probabilistic\n");
    for(l = 0; l < L; l++){
      LSH_Ar[l] = createHashTable(numCurves);
    }
  }
  if(pinfo->f == 1){
    fprintf(outf,"Distance Function: DFT\n" );
  }
  else if(pinfo->f == 2){
    fprintf(outf,"Distance Function: DTW\n" );
  }

  while ((read = getline(&line, &len, stream)) != -1) {
      line[read - 1] = 0;
      char *pch;

      curvePtr curveP = malloc(sizeof(Curve));
      if(curveP == NULL)
      {
            printf("Error! memory not allocated.");
            EXIT_FAILURE;
      }

      pch = strtok(line, "\t");
      curveP->id = atoi(pch);
      //fprintf(outf,"id %d\n", curve.id);

      pch = strtok(NULL, "\t");
      pch[strlen(pch)] = 0;
      curveP->points = atoi(pch);
      //fprintf(outf,"points %d\n", curve.points);

      curveP->coordinates = (long double*)malloc(D * curveP->points * sizeof(long double));
      if(curveP->coordinates == NULL)
      {
            printf("Error! memory not allocated.");
            EXIT_FAILURE;
      }
      for(i=0; i<curveP->points*D; i++){
        if(i <= 1){
          pch = strtok(NULL, ",");
          pch[strlen(pch) - 1] = 0;
          pch++;
          curveP->coordinates[i] = strtold(pch, NULL);

          i++;

          pch = strtok(NULL, ",");
          pch[strlen(pch) - 1] = 0;
          pch++;
          curveP->coordinates[i] = strtold(pch, NULL);
          j=2;
        }else{
          flag=0;
          pch = strtok(NULL, ",");
          pch[strlen(pch) - 1] = 0;
          pch += 2;
          temp = strtold(pch, NULL);
        //  fprintf(outf,"temp %.15Lf \n",temp);
          if(curveP->coordinates[j-2] == temp){
            //fprintf(outf,"temp mesa %.15Lf \n",temp);
            flag++;
          }
          i++;

          pch = strtok(NULL, ",");
          pch[strlen(pch) - 1] = 0;
          pch++;
          if((curveP->coordinates[j-1] = strtold(pch, NULL)) && flag==1){
            flag++;
            //fprintf(outf,"x dup %.15Lf y dup %.15Lf \n",temp,curveP->coordinates[j] = strtold(pch, NULL));
          }else{
            //fprintf(outf,"easy pass\n");
            curveP->coordinates[j]=temp;
            curveP->coordinates[j+1] = strtold(pch, NULL);
            j+=2;
            //fprintf(outf,"x %.15Lf y %.15Lf \n",curveP->coordinates[j-2],curveP->coordinates[j-1] = strtold(pch, NULL));
          }
          if(flag == D){
            //fprintf(outf," dups are %.15Lf %.15Lf %.15Lf %.15Lf \n",curveP->coordinates[i-1],curveP->coordinates[i],curveP->coordinates[i-3],curveP->coordinates[i-2]);
            duplicates++;
          }
        }
      }
      curveP->points=curveP->points-duplicates;
      // fprintf(outf,"New points %d\n",curveP->points);
      // fprintf(outf,"dups %d\n",duplicates);
      curveP->coordinates = (long double*)realloc(curveP->coordinates ,D * curveP->points * sizeof(long double));
      duplicates = 0;

      //finished reading curve
      curveAr[position] = curveP;   //saved to array
      position++;
  }
  fclose(stream);
  free(line);
  //ar_ri
  int ar_ri[K*max*D];
  int myRand,range;
  int rangeLow=0;
  int rangeHigh=10000;
  for(i=0;i<K*max*D;i++){
    myRand = (int)rand();
    range = rangeHigh - rangeLow + 1;
    ar_ri[i] = (myRand % range) + rangeLow;
  }
  for(i = 0; i < numCurves; i++){
    g = grid(curveAr[i], delta, pinfo->K);
    if(pinfo->h == 1){
      for(l = 0; l < L; l++){
        classicHash(g, max, numCurves, LSH_Ar[l], curveAr[i]->id, ar_ri);
      }
    }
    else if(pinfo->h == 2){
      for(l = 0; l < L; l++){
        LSH_Hash(g, numCurves, LSH_Ar[l], curveAr[i]->id, ar_ri);
      }
    }
  }

  for(l = 0; l < L; l++){
    // printHashTable(LSH_Ar[l], numCurves);
  }
  query(curveAr, LSH_Ar, delta, max, numCurves, ar_ri, pinfo->query, pinfo->K, pinfo->L, pinfo->f, outf);

  for(l = 0; l < L; l++){
    freeHashTable(LSH_Ar[l], numCurves, l);
  }
  free(LSH_Ar);

  for(i = 0; i < numCurves; i++){
    free(curveAr[i]->coordinates);
    free(curveAr[i]);
  }
  free(curveAr);
  free(pinfo);
  clock_t end = clock();
  double time_spent = (double)(end-begin)/CLOCKS_PER_SEC;
  fprintf(outf,"time spent %f\n", time_spent);
  fclose(outf);

  return 0;
}
