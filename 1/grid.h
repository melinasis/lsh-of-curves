#include <stdio.h>
#include <stdlib.h>

typedef struct Curve* curvePtr;

typedef struct Curve{
  int id;
  int points;
  long double* coordinates;
}Curve;


typedef struct node* nodePtr; 						//pointer to node

typedef struct node{
	long double point;
	struct node* next;
}node;

long double uniformlyRand(long double delta);
nodePtr grid(curvePtr curveP, long double delta, int K);
long double gridpoint(long double p, long double t, long double delta);
nodePtr createGridList();
void push(nodePtr head, long double x, long double y);
void concatenate(nodePtr head, nodePtr newList);
void printList(nodePtr head);
int countNode(nodePtr head);
int removeDuplicates(nodePtr head, long double x, long double y);
void freeGridList(nodePtr head);

nodePtr createHlist();
