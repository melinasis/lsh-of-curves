#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "grid.h"
#include "prog.h"

nodePtr grid(curvePtr curveP, long double delta, int K){
  int i, k;
  long double t, p, x, y;
  nodePtr currentList, finalList;
  for(k = 0; k < K; k++){
    // printf("K %d\n", k);
    //create a list
    currentList = createGridList();

    t = uniformlyRand(delta);
    for(i=0; i<curveP->points*D; i++){
      // printf("Point ready to be modified: %.15Lf\n", curveP->coordinates[i]);
      // printf("t is %.15Lf\n", t);
      p = gridpoint(curveP->coordinates[i], t, delta);
      // printf("gridpoint is %Lf\n", p);
      if(i%2 == 0){ //x
        x = p;
      }
      else{
        y = p;
        if(removeDuplicates(currentList, x, y) == 0){
          push(currentList, x, y);
        }
      }
    }//all points modified as grid points
    // printList(currentList);
    if(k > 0){//a list is already created
        concatenate(finalList, currentList);
    }
    else
      finalList = currentList;
  }
  // printf("Concatenation:\n");
  // printList(finalList);
  return finalList;
}

long double gridpoint(long double p, long double t, long double delta){
    int ai;
    long double a1, a2, dista, distb, change = 0;
    if(p < 0) {
      p = -p;
      change = 1;
    }
    ai = (int)(p / (delta + t));
    // printf("ai %d\n",ai );
    a1 = ai*(delta + t);
    // printf("a1 %Lf\n",a1 );
    a2 = a1 + delta + t;
    // printf("a2 %Lf\n",a2 );

    dista = p - a1;
    // printf("dist1 %Lf\n",dista );
    distb = a2 - p;
    // printf("dist2 %Lf\n",distb );

    if(dista < distb){
      if(change == 1) a1 = -a1;
      return a1;
    }
    else{
      if(change == 1) a2 = -a2;
      return a2;
    }
}

long double uniformlyRand(long double delta) {
	long double rangeLow = 0.0, rangeHigh = delta;
  long double myRand;
  myRand = rangeHigh + (rand() / (RAND_MAX + 1.0))*(rangeHigh - rangeLow +1);
	return myRand;
}

nodePtr createGridList(){
  nodePtr gridList = malloc(sizeof(node));
  if(gridList == NULL)
  {
        printf("Error! memory not allocated.");
        EXIT_FAILURE;
  }
  gridList->point = 0.0;
  gridList->next = malloc(sizeof(node));
  if(gridList->next == NULL)
  {
        printf("Error! memory not allocated.");
        EXIT_FAILURE;
  }
  gridList->next->point = 0.0;
  gridList->next->next = NULL;
  return gridList;
}

void push(nodePtr head, long double x, long double y) {
    nodePtr current = head;
    if(current->point == 0.0){    //first two nodes
      current->point = x;
      current->next->point = y;
    }
    else{
      while (current->next->next != NULL) {
          current = current->next->next;
      }
      /* now we can add two nodes */
      current->next->next = malloc(sizeof(node));
      if(current->next->next == NULL)
      {
            printf("Error! memory not allocated.");
            EXIT_FAILURE;
      }
      current->next->next->point = x;
      current->next->next->next = malloc(sizeof(node));
      if(current->next->next->next == NULL)
      {
            printf("Error! memory not allocated.");
            EXIT_FAILURE;
      }
      current->next->next->next->point = y;
      current->next->next->next->next = NULL;
    }

}

void concatenate(nodePtr head, nodePtr newList){
  nodePtr current = head;
  while (current->next != NULL) {
      current = current->next;
  }
    current->next = newList;
}

void printList(nodePtr head){
  nodePtr current = head;
  printf("%.15Lf -> ", current->point);
  while(current->next != NULL){
    current = current->next;
    printf("%.15Lf -> ", current->point);
  }
  printf("NULL\n");
  printf("Number of nodes: %d\n", countNode(head));
}
int removeDuplicates(nodePtr head, long double x, long double y){
  int dup = 0;
  nodePtr current = head;
  nodePtr final_X;

  while(current->next->next != NULL){
      current = current->next->next;
  }
  final_X = current;
  if(final_X->point == x && final_X->next->point == y){
    dup = 1;
    // printf("Duplicate\n");
  }
  return dup;
}

int countNode(nodePtr head){
  int count = 1;
  nodePtr current = head;

  while(current->next != NULL)
  {
    current = current->next;
    count++;
    //printf("%d\n", count);
  }
  return count;
}

void freeGridList(nodePtr head){
  nodePtr temp;
  nodePtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}
