#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define D 2  //dimension
#define KVEC 3

typedef struct Prog* Pinfo;
typedef struct Prog{
	int K, L, h, f, s;
	char input[20], query[20], hash[15], fd[3], output[20];
}Prog;

int argHandling(Pinfo pinfo, int argc, char **argv);
void printHelp(char *progName);
