#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "prog.h"
#include "grid.h"
#include "hashTable.h"

void query(curvePtr* curveAr, BucketPtr** LSH_Ar, long double delta, int max, int numCurves, int ar_ri[], char* queryFile, int K, int L, int f, FILE *outf){
  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int i, j, flag, l, min_id, maxBucket, temp1, k, found, q;
  int duplicates = 0, sumMaxBucket = 0, complete_meter = 0, counter = 1, sumQuerys = 0;
  double R;
  long double temp, min = -1.0, min_dist;
  nodePtr query_g;
  curvePtr  initialCurve;
  Neigh* completeNeighR;
  Neigh* neighR;
  Neigh neigh;
  curvePtr queryCurve;


  stream = fopen(queryFile, "r" );
  fprintf(outf,"\n" );
  while ((read = getline(&line, &len, stream)) != -1) {
      fprintf(outf,"\n" );
      complete_meter = 0;
      line[read - 1] = 0;
      char *pch;

      // fprintf(outf,"new query\n" );
      if(counter == 1){
      	pch=strtok(line," ");
      	pch=strtok(NULL," ");
      	R=atof(pch);
      	fprintf(outf,"R = %lf\n",R);
	       counter++;
      }
      else{
        curvePtr queryCurve = malloc(sizeof(Curve));
        if(queryCurve == NULL)
        {
              printf("Error! memory not allocated.");
              EXIT_FAILURE;
        }
        pch = strtok(line, "\t");
        queryCurve->id = atoi(pch);
        //fprintf(outf,"id %d\n", curve.id);

        pch = strtok(NULL, "\t");
        pch[strlen(pch)] = 0;
        queryCurve->points = atoi(pch);
        //fprintf(outf,"points %d\n", curve.points);

        queryCurve->coordinates = (long double*)malloc(D * queryCurve->points * sizeof(long double));
        if(queryCurve->coordinates == NULL)
        {
              printf("Error! memory not allocated.");
              EXIT_FAILURE;
        }
        for(i=0; i<queryCurve->points*D; i++){
          if(i <= 1){
            pch = strtok(NULL, ",");
            pch[strlen(pch) - 1] = 0;
            pch++;
            queryCurve->coordinates[i] = strtold(pch, NULL);

            i++;

            pch = strtok(NULL, ",");
            pch[strlen(pch) - 1] = 0;
            pch++;
            queryCurve->coordinates[i] = strtold(pch, NULL);
            j=2;
          }else{
            flag=0;
            pch = strtok(NULL, ",");
            pch[strlen(pch) - 1] = 0;
            pch += 2;
            temp = strtold(pch, NULL);
          //  fprintf(outf,"temp %.15Lf \n",temp);
            if(queryCurve->coordinates[j-2] == temp){
              //fprintf(outf,"temp mesa %.15Lf \n",temp);
              flag++;
            }
            i++;

            pch = strtok(NULL, ",");
            pch[strlen(pch) - 1] = 0;
            pch++;
            if((queryCurve->coordinates[j-1] = strtold(pch, NULL)) && flag==1){
              flag++;
              //fprintf(outf,"x dup %.15Lf y dup %.15Lf \n",temp,curveP->coordinates[j] = strtold(pch, NULL));
            }else{
              //fprintf(outf,"easy pass\n");
              queryCurve->coordinates[j]=temp;
              queryCurve->coordinates[j+1] = strtold(pch, NULL);
              j+=2;
              //fprintf(outf,"x %.15Lf y %.15Lf \n",curveP->coordinates[j-2],curveP->coordinates[j-1] = strtold(pch, NULL));
            }
            if(flag == D){
              //fprintf(outf," dups are %.15Lf %.15Lf %.15Lf %.15Lf \n",curveP->coordinates[i-1],curveP->coordinates[i],curveP->coordinates[i-3],curveP->coordinates[i-2]);
              duplicates++;
            }
          }
        }
        queryCurve->points=queryCurve->points-duplicates;
        // fprintf(outf,"New points %d\n",curveP->points);
        // fprintf(outf,"dups %d\n",duplicates);
        queryCurve->coordinates = (long double*)realloc(queryCurve->coordinates ,D * queryCurve->points * sizeof(long double));
        duplicates = 0;


        //finished reading curve
        query_g = grid(queryCurve, delta, K);
        int key = keyValue(query_g, max, numCurves, ar_ri);
        // fprintf(outf,"KEY is %d\n", key);

        //find sumMaxBucket
        sumMaxBucket = 0;
        for(l = 0; l < L; l++){
          BucketPtr* curHashTable = LSH_Ar[l];
          maxBucket = maxBuck(curHashTable[key]);
          // fprintf(outf,"maxBucket %d\n", maxBucket );
          sumMaxBucket += maxBucket;
        }


        // fprintf(outf,"sumMaxBucket %d\n", sumMaxBucket);
        if(sumMaxBucket == 0){
          found = -1;
          fprintf(outf,"Query: %d\n", queryCurve->id);
          fprintf(outf,"Bucket is empty(all hashtables). LSH Neighbor cannot be found.\n" );
          neigh = malloc(sizeof(Neighbor));
          if(neigh == NULL)
          {
                printf("Error! memory not allocated.");
                EXIT_FAILURE;
          }
        }
        else{
          completeNeighR = malloc(sumMaxBucket * sizeof(Neigh));
          if(completeNeighR == NULL)
          {
                printf("Error! memory not allocated.");
                EXIT_FAILURE;
          }
          for(i = 0; i < sumMaxBucket; i++){
            completeNeighR[i] = malloc(sizeof(Neighbor));
            if(completeNeighR[i] == NULL)
            {
                  printf("Error! memory not allocated.");
                  EXIT_FAILURE;
            }
            completeNeighR[i]->dist = -1.0;
          }
          neigh = malloc(sizeof(Neighbor));
          if(neigh == NULL)
          {
                printf("Error! memory not allocated.");
                EXIT_FAILURE;
          }

          for(l = 0; l < L; l++){
            // fprintf(outf,"SEARCH IN HASHTABLE %d\n",l );
            BucketPtr* curHashTable = LSH_Ar[l];
            maxBucket = maxBuck(curHashTable[key]);

            neighR = malloc(maxBucket * sizeof(Neigh));
            if(neighR == NULL)
            {
                  printf("Error! memory not allocated.");
                  EXIT_FAILURE;
            }
            for(i = 0; i < maxBucket; i++){
              neighR[i] = malloc(sizeof(Neighbor));
              if(neighR[i] == NULL)
              {
                    printf("Error! memory not allocated.");
                    EXIT_FAILURE;
              }
              neighR[i]->dist = -1.0;
            }

              //search in bucket
              // fprintf(outf,"Search in bucket\n" );
              found = search(LSH_Ar[l], key, query_g, numCurves, curveAr, queryCurve, neighR, neigh, f, R);
              if(found == 1){
                // fprintf(outf,"found\n" );
                //keep the minimum of l hashtables
                if(min == -1.0){
                  min = neigh->dist;
                  min_id = neigh->n_id;
                }
                else if(neigh->dist < min || min == 0.0){
                  if(neigh->dist != 0.0){
                    min = neigh->dist;
                    min_id = neigh->n_id;
                  }
                }

                if(R > 0.0){
                  for(i = 0; i < maxBucket; i++){
                    if(neighR[i]->dist != -1.0){

                      completeNeighR[complete_meter]->dist = neighR[i]->dist;
                      completeNeighR[complete_meter]->n_id = neighR[i]->n_id;
                      complete_meter++;
                    }
                  }
                }
              }
              for(i = 0; i < maxBucket; i++){
                free(neighR[i]);
              }
              free(neighR);
            }

        }//next hashtable

      if(found == 0){
        min = -1.0;
        // fprintf(outf,"Check all bucket\n" );
        for(l = 0; l < L; l++){
          // fprintf(outf,"SEARCH IN HASHTABLE %d\n",l );

          neighR = malloc(maxBucket * sizeof(Neigh));
          if(neighR == NULL)
          {
                printf("Error! memory not allocated.");
                EXIT_FAILURE;
          }
          for(i = 0; i < maxBucket; i++){
            neighR[i] = malloc(sizeof(Neighbor));
            if(neighR[i] == NULL)
            {
                  printf("Error! memory not allocated.");
                  EXIT_FAILURE;
            }
            neighR[i]->dist = -1.0;
          }


          searchAllBucket(LSH_Ar[l], key, query_g, numCurves, curveAr, queryCurve, neighR, neigh, f, R);
          // fprintf(outf,"dist %Lf n_id %d\n", neigh->dist, neigh->n_id);
          if(min == -1.0){
              min = neigh->dist;
              min_id = neigh->n_id;
          }
          else if(neigh->dist < min || min == 0.0){
            if(neigh->dist != 0.0){
              min = neigh->dist;
              min_id = neigh->n_id;
            }
          }


          if(R > 0.0){
            for(i = 0; i < maxBucket; i++){
              if(neighR[i]->dist != -1.0){
                // fprintf(outf,"neighR[%d]->dist %Lf\n",i, neighR[i]->dist);
                completeNeighR[complete_meter]->dist = neighR[i]->dist;
                completeNeighR[complete_meter]->n_id = neighR[i]->n_id;
                // fprintf(outf,"completeNeighR[%d] = neighR[%d] \n", complete_meter, i);
                complete_meter++;
              }
            }
          }
          for(i = 0; i < maxBucket; i++){
            free(neighR[i]);
          }
          free(neighR);
        }
      }

      //RESULTS
      if(found == 1){
        // fprintf(outf,"found\n" );
        // fprintf(outf,"LSH NEIGHBOR!\n");
        fprintf(outf,"Query: %d\n", queryCurve->id);
        fprintf(outf,"FoundGridCurve: True\n" );
        fprintf(outf,"LSH Nearest Neighbor curve%d\n", min_id);
        fprintf(outf,"distanceLSH: %Lf\n", neigh->dist);

      }
      else if(found == 0){
        // fprintf(outf,"LSH NEIGHBOR!\n");
        fprintf(outf,"Query: %d\n", queryCurve->id);
        fprintf(outf,"FoundGridCurve: False\n" );
        //check all bucket
        fprintf(outf,"LSH Nearest Neighbor: curve%d\n", min_id);
        fprintf(outf,"distanceLSH: %Lf\n", neigh->dist);
      }


      //SEARCH TRUE
      min = -1.0;
      for(l = 0; l < L; l++){
        searchTrue(LSH_Ar[l], query_g, numCurves, curveAr, queryCurve, neigh, f);

        if(min == -1.0){
            min = neigh->dist;
            min_id = neigh->n_id;
        }
        else if(neigh->dist < min || min == 0.0){
          if(neigh->dist != 0.0){
            min = neigh->dist;
            min_id = neigh->n_id;
          }
        }
      }

      //RESULTS
      fprintf(outf,"True Nearest Neighbor: curve%d\n", min_id);
      fprintf(outf,"distanceTrue: %Lf\n", neigh->dist);

      if(found != -1){
        //SEARCH R
        if(R > 0.0){
          int R_Curves[sumMaxBucket];
          fprintf(outf,"R-near neighbors:\n");
          for(i = 0; i < sumMaxBucket; i++){
            if(completeNeighR[i]->dist != -1.0){
              R_Curves[i] = completeNeighR[i]->n_id;
            }
            else{
              R_Curves[i] = -1;
            }
          }//R_Curves filled
          //remove duplicate
          temp1 = sumMaxBucket;
          for (i = 0; i < temp1; i++) {
            for (j = i + 1; j < temp1;) {
              if (R_Curves[j] == R_Curves[i]) {
               for (k = j; k < temp1; k++) {
                  R_Curves[k] = R_Curves[k + 1];
               }
               temp1--;
             } else
               j++;
             }
          }
          for (i = 0; i < temp1; i++) {
            if(R_Curves[i] != -1)
             fprintf(outf,"curve%d\n", R_Curves[i]);
          }
        }

        for(i = 0; i < sumMaxBucket; i++){
          if(completeNeighR[i] != NULL){
            free(completeNeighR[i]);
          }
        }
        free(completeNeighR);
        free(neigh);
        free(queryCurve->coordinates);
        free(queryCurve);
        freeGridList(query_g);
      }
    }
  }//next query


  fclose(stream);
  free(line);
}

int search(BucketPtr* HashTable, int key, nodePtr query_g, int numCurves, curvePtr* curveAr, curvePtr queryCurve, Neigh* neighR, Neigh neigh, int f, double R){
  nodePtr current_g, current_qg;
  long double current_point, current_qpoint;
  int id, i, min_id;
  int not_equal = 0, found = 0, meter = 0;
  long double dist, min = -1.0;
  curvePtr initialCurve;

  BucketPtr current = HashTable[key];

  while(current != NULL && current->id != -1){
    current_g = current->g;
    current_qg = query_g;
    // fprintf(outf,"g" );
    // printList(current_g);
    // fprintf(outf,"query g" );
    // printList(current_qg);
    // fprintf(outf,"\n" );
    //search bucket for equal grid curves
    while(current_g != NULL && current_qg != NULL){
      current_point = (int)current_g->point;
      current_qpoint = (int)current_qg->point;
      if(current_point == current_qpoint){
        current_g = current_g->next;
        current_qg = current_qg->next;
      }
      else{
        // fprintf(outf,"NOT EQUAL\n" );
        not_equal = 1;
        break;
      }
    }
    if(not_equal == 0){
      // fprintf(outf,"BOOM\n" );
      found = 1;
      id = current->id;
      //search in curveAr for that id
      for(i = 0; i < numCurves; i++){
        if(curveAr[i]->id == id){
          initialCurve = curveAr[i];
          break;
        }
      }

      if(f == 1) dist = DFD_function(initialCurve, queryCurve);
      else if(f == 2) dist = DTW_function(initialCurve, queryCurve);

      //find min of equal grid curves
      if(min == -1.0){
        min = dist;
        min_id = initialCurve->id;
      }
      else if(dist < min || min == 0.0){
        if(dist != 0.0){
          min = dist;
          min_id = initialCurve->id;
        }
      }

      if(R > 0.0){ //keep all distances < R
        if(dist <= R){
          neighR[meter]->dist = dist;
          neighR[meter]->n_id = initialCurve->id;
          meter++;
        }
      }

      // fprintf(outf,"min = %Lf\n", min);
    }
    //check next grid curve in the bucket
    current = current->next;
    not_equal = 0;
  }
  if(found == 1){
    neigh->dist = min;
    neigh->n_id = min_id;
  }
  return found;
}

void searchAllBucket(BucketPtr* HashTable, int key, nodePtr query_g, int numCurves, curvePtr* curveAr, curvePtr queryCurve, Neigh* neighR, Neigh neigh, int f, double R){
  int id, i, min_id,  meter = 0;
  long double dist, min = -1.0;
  curvePtr initialCurve;

  BucketPtr current = HashTable[key];
  while(current != NULL && current->id != -1){
    id = current->id;
    //search in curveAr for that id
    for(i = 0; i < numCurves; i++){
      if(curveAr[i]->id == id){
        initialCurve = curveAr[i];
        break;
      }
    }
    if(f == 1) dist = DFD_function(initialCurve, queryCurve);
    else if(f == 2) dist = DTW_function(initialCurve, queryCurve);

    if(min == -1.0){
      min = dist;
      min_id = initialCurve->id;
    }
    else if(dist < min || min == 0.0){
      if(dist != 0.0){
        min = dist;
        min_id = initialCurve->id;
      }
    // fprintf(outf,"min = %Lf\n", min);
    }
    if(R > 0.0){
      if(dist <= R){
        neighR[meter]->dist = dist;
        neighR[meter]->n_id = initialCurve->id;
        meter++;
      }
    }
    //check next grid curve in the bucket
    current = current->next;
  }

  neigh->dist = min;
  neigh->n_id = min_id;


  return;
}

void searchTrue(BucketPtr* HashTable, nodePtr query_g, int numCurves, curvePtr* curveAr, curvePtr queryCurve, Neigh neigh, int f){
  int id, i, min_id, key;
  long double dist, min = -1.0;
  curvePtr initialCurve;


  for(key = 0; key < numCurves/2; key++){
    BucketPtr current = HashTable[key];
    while(current != NULL && current->id != -1){

      id = current->id;
      //search in curveAr for that id
      for(i = 0; i < numCurves; i++){
        if(curveAr[i]->id == id){
          initialCurve = curveAr[i];
          break;
        }

      }
      if(f == 1) dist = DFD_function(initialCurve, queryCurve);
      else if(f == 2) dist = DTW_function(initialCurve, queryCurve);
      // dist = DTW_function(initialCurve, queryCurve);
      if(min == -1.0){
        min = dist;
        min_id = initialCurve->id;
      }
      else if(dist < min || min == 0.0){
        if(dist != 0.0){
          min = dist;
          min_id = initialCurve->id;
        }
      }
        // fprintf(outf,"min = %Lf\n", min);

      //check next grid curve in the bucket
      current = current->next;
    }

    neigh->dist = min;
    neigh->n_id = min_id;
  }
}
