#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "prog.h"

void printHelp(char *progName) {
    printf("Usage: %s arguments\n\
\033[22;33m-d\033[m [filename]  input file\n\
\033[22;33m-q\033[m [filename]  query file\n\
\033[22;33m-k\033[m  [optional][number]   grid curves\n\
\033[22;33m-L\033[m  [optional][number]   hash tables\n\
\033[22;33m-o\033[m  [filename] output file\n\
\033[22;33m-f\033[m  [DFD or DTW]\n\
\033[22;33m-h\033[m      [classic or propabilistic]\n", progName);

}

int argHandling(Pinfo pinfo, int argc, char **argv) {
    int argIter = 1;
    if(argc == 1) return EXIT_FAILURE;
    pinfo->K = 2; //default
    pinfo->L = 3; //default
    pinfo->h = 0;
    pinfo->f = 0;
    while (argIter < argc) {
        if (argv[argIter][0] == '-') {
            if (argv[argIter][2] == '\0') {
                switch (argv[argIter][1]) {
                  case 'd':
                       if (++argIter == argc)
                           break;
                       strcpy(pinfo->input, argv[argIter]);
                       break;
                   case 'q':
                       if (++argIter == argc)
                           break;
                       strcpy(pinfo->query, argv[argIter]);
                       break;
                  case 'k':
                       if (++argIter == argc)
                           break;
                       pinfo->K = atoi(argv[argIter]);
                       break;
                   case 'L':
                       if (++argIter == argc)
                           break;
                       pinfo->L = atoi(argv[argIter]);
                       break;
                  case 'o':
                      if (++argIter == argc)
                          break;
                      strcpy(pinfo->output, argv[argIter]);
                      break;
                   case 'f':
                       if (++argIter == argc)
                           break;
                       strcpy(pinfo->fd, argv[argIter]);
                       if(strcmp(pinfo->fd, "DFD") == 0){
                          pinfo->f = 1;
                      }
                      else if(strcmp(pinfo->fd, "DTW") == 0){
                          pinfo->f= 2;
                      }
                      break;
                  case 'h':
                      if (++argIter == argc)
                          break;
                      strcpy(pinfo->hash, argv[argIter]);
                      if(strcmp(pinfo->hash, "classic") == 0){
                        pinfo->h = 1;
                      }
                      else if(strcmp(pinfo->hash, "propabilistic") == 0){
                        pinfo->h = 2;
                      }
                      break;
                  default:
                      return EXIT_FAILURE;
              }
          }
          else{
            return EXIT_FAILURE;
          }
      }
      else{
        return EXIT_FAILURE;
      }
        ++argIter;
    }

    if(pinfo->h == 0 || pinfo->f == 0 || strlen(pinfo->input) == 0 || strlen(pinfo->query) == 0){
      return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
