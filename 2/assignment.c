#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cluster.h"
#include "prog.h"
#include "grid.h"
#include "hashTable.h"


void assignmentRangeSearch(int* ar_ri, int loop, BucketPtr** LSH_Ar, curvePtr* centroidAr, curvePtr* curveAr, Pinfo pinfo, int** centroidL){

  int i, l, c, key, a;
  int rep = 0, stop = 0, cen_in_bucket = 0;
  nodePtr g;

  int K = pinfo->K;
  int L = pinfo->L;
  int D = pinfo->D;
  int clusters = pinfo->clusters;
  int numCurves = pinfo->numCurves;
  int maxPoints = pinfo->maxPoints;

  if(loop == 0){

    //index all (centroid and non centroid) points
    //keep position of centroids in hashtables
    for(i = 0; i < numCurves; i++){
      g = grid(curveAr[i], pinfo);
      if(curveAr[i]->is_centroid == 1){
      //   cenBuckets constructed only for centroids
      //   for each centroid keep the number of bucket(key) that it hashed into
        curveAr[i]->cenBuckets = malloc(L * sizeof(int));
        if(curveAr[i]->cenBuckets == NULL){
              printf("Error! memory not allocated.");
              return;
        }
      }
      for(l = 0; l < L; l++){
        LSH_Hash(g, numCurves, LSH_Ar[l], curveAr[i], ar_ri, D, l);
      }
    }
  }
  else{
    for(c = 0; c < clusters; c++){
      if(centroidAr[c]->id >= numCurves){
        g = grid(centroidAr[c], pinfo);

        centroidAr[c]->cenBuckets = malloc(L * sizeof(int));
        if(centroidAr[c]->cenBuckets == NULL){
              printf("Error! memory not allocated.");
              return;
        }
        for(l = 0; l < L; l++){
          LSH_Hash(g, numCurves, LSH_Ar[l], centroidAr[c], ar_ri, D, l);
        }
      }
      else{

        for(l = 0; l < L; l++){
          centroid_flag(LSH_Ar[l], numCurves, centroidAr[c], l);
        }
      }
    }
  }
  //move centroids at the start of each bucket
  for(l = 0; l < L; l++){
    moveCentroidsStart(LSH_Ar[l], pinfo, centroidAr, l);
  }

  // printf("Initialization of centroids is complete.\n" );
  // for(l = 0; l < L; l++){
  //   printHashTable(LSH_Ar[l], numCurves);
  //   printf("********************************************************************************\n");
  // }
  //begin range search
  long double R = minDistCen(centroidAr, pinfo) / 2;
  // printf("R %Lf\n", R);


  //centroidL keeps number of centroids of each bucket in all (L) HashTables
  //fill centroidL
  BucketPtr* HashTable;
  BucketPtr current;
  int* currentCentroidAr;
  for(l = 0; l < L; l++){//for each HashTable
    HashTable = LSH_Ar[l];
    currentCentroidAr = centroidL[l];
    for(i = 0; i < numCurves/8; i++){ //for each bucket
      current = HashTable[i];
      while(current->is_centroid == 1){
        cen_in_bucket++;
        if(current->next != NULL){
          current = current->next;
        }
      }
      currentCentroidAr[i] = cen_in_bucket;
      cen_in_bucket = 0;
      //next bucket
    }
    //next hashTable
  }

  while(stop < L){
    stop = 0;
    for(l = 0; l < L; l++){
        //search in bucket
        stop += rangeSearchBucket(LSH_Ar[l], l, R, pinfo, curveAr, centroidAr, centroidL[l]);
        //if stop == L all hashtables stayed the same
    }
    if(stop < L){//something changed
      // printf("ASSIGMENT %d\n", rep);
      // for(l = 0; l < L; l++){
      //   printHashTable(LSH_Ar[l], numCurves);
      //   printf("********************************************************************************\n");
      // }
      rep++;
      R = R*2;
    }
  }
  //all curves in buckets with centroids are assigned to a centroid

  //assign any unassigned curve
  checkUnassigned(curveAr, centroidAr, pinfo);
  // printf("ASSIGN ALL\n" );
  // for(a = 0; a < L; a++){
  //   printHashTable(LSH_Ar[a], numCurves);
  //   printf("********************************************************************************\n");
  // }

  for(l = 0; l < L; l++){
    resetHashTable(LSH_Ar[l], numCurves);
  }
}

long double minDistCen(curvePtr* centroidAr, Pinfo pinfo){
  int i;

  long double dist, min = 0.0;
  for(i = 0; i < pinfo->clusters; i++){
    if(i == pinfo->clusters-1)  break;
    curvePtr curve1 = centroidAr[i];
    curvePtr curve2 = centroidAr[i+1];
    if(pinfo->f == 1) dist = DFD_function(curve1, curve2, pinfo->D);
    else if(pinfo->f == 2) dist = DTW_function(curve1, curve2, pinfo->D);

    // printf("Distance between %d and %d: %Lf\n",curve1->id,curve2->id , dist);
    if(min == 0.0)  min = dist;
    else if(dist < min) min = dist;
  }
  return min;
}

int rangeSearchBucket(BucketPtr* HashTable, int l, double R, Pinfo pinfo, curvePtr* curveAr, curvePtr* centroidAr, int* currentCentroidAr){
  int i, j, cen_id, query_id, cur, cen, stop = 1;
  int cen_in_bucket = 0;
  curvePtr queryCurve, centroidCurve;
  long double dist, min = 0.0;
  BucketPtr current, cur_cen;

  for(i = 0; i < pinfo->numCurves/8; i++){ //for each bucket
    cen_in_bucket = currentCentroidAr[i];
    // printf("Bucket %d: %d centroids\n", i, cen_in_bucket);
    if(cen_in_bucket > 0){
      current = HashTable[i]; //start
      //jump centroids
      for(j = cen_in_bucket; j > 0; j--){
        current = current->next;
      }
      while(current != NULL){ //check all bucket
        //query curve id
        query_id = current->id;
        //query curve pointer
        for(cur = 0; cur < pinfo->numCurves; cur++){
          if(curveAr[cur]->id == query_id){
            //check if it has already a center
            if(curveAr[cur]->has_centroid == -1){
              queryCurve = curveAr[cur];
            }
            else{
              // printf("Curve %d already has a center (%d)\n",query_id, curveAr[cur]->has_centroid);
              current->has_centroid = curveAr[cur]->has_centroid;
            }
            break;
          }
        }
        //check for flag first
        if(current->has_centroid == -1){
          stop = 0;

          //find dist for each centroid
          cur_cen = HashTable[i];
          for(j = 0; j < cen_in_bucket; j++){
            //centroid curve id
            cen_id = cur_cen->id;
            //centroid curve pointer
            for(cen = 0; cen < pinfo->clusters; cen++){
              if(centroidAr[cen]->id == cen_id){
                centroidCurve = centroidAr[cen];
                break;
              }
            }

            //Distance
            if(pinfo->f == 1) dist = DFD_function(centroidCurve, queryCurve, pinfo->D);
            else if(pinfo->f == 2) dist = DTW_function(centroidCurve, queryCurve, pinfo->D);

            // printf("Distance between \033[22;31m%d\033[m and %d: %Lf\n",centroidCurve->id, queryCurve->id , dist);

            if(dist <= R){
              if(min == 0.0){
                min = dist;
                current->has_centroid = cen_id;
                queryCurve->has_centroid = cen_id;
              }
              else if(dist < min){
                min = dist;
                current->has_centroid = cen_id;
                queryCurve->has_centroid = cen_id;
              }
            }

            //next centroid
            min = 0.0;
            cur_cen = cur_cen->next;
          }
        }
        //next curve
        current = current->next;
      }
    }
    cen_in_bucket = 0;
    //next bucket
  }
  return stop;
}

void checkUnassigned(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo){
  int i, query_id, cur, cen_id, j;
  BucketPtr current;
  curvePtr queryCurve;
  long double dist, min = 0.0;

  for(cur = 0; cur < pinfo->numCurves; cur++){
    if(curveAr[cur]->is_centroid == -1 && curveAr[cur]->has_centroid == -1){
      // printf("curve %d is unassigned\n", curveAr[cur]->id);
      queryCurve = curveAr[cur];

      //for each centroid
      for(j = 0; j < pinfo->clusters; j++){
        //Distance
        if(pinfo->f == 1) dist = DFD_function(centroidAr[j], queryCurve, pinfo->D);
        else if(pinfo->f == 2) dist = DTW_function(centroidAr[j], queryCurve, pinfo->D);

        if(min == 0.0){
          min = dist;
          queryCurve->has_centroid = centroidAr[j]->id;
        }
        else if(dist < min){
          min = dist;
          queryCurve->has_centroid = centroidAr[j]->id;
        }
      }
      min = 0.0;
    }
  }
}

//Lloyd's
void lloyds(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo){
  fflush(stdout);
  int D = pinfo->D;
  int k = pinfo->clusters;
  int numCurves = pinfo->numCurves;
  long double dist, min = 0.0;
  int i, j;


  for(i = 0; i < numCurves; i++){//for each curve
    if(curveAr[i]->is_centroid == -1){//non centroid
      for(j = 0; j < k; j++){//for each centroid
        //Distance
        if(pinfo->f == 1) dist = DFD_function(centroidAr[j], curveAr[i], pinfo->D);
        else if(pinfo->f == 2) dist = DTW_function(centroidAr[j], curveAr[i], pinfo->D);

        //keep min
        if(min == 0.0){
          min = dist;
          curveAr[i]->has_centroid = centroidAr[j]->id;
        }
        else if(dist < min){
          min = dist;
          curveAr[i]->has_centroid = centroidAr[j]->id;
        }
      }
    }
    //next curve
    min = 0.0;
  }
}
