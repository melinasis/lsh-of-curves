#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cluster.h"
#include "grid.h"
#include "prog.h"

void randomInit(Pinfo pinfo, curvePtr* curveAr, curvePtr* centroidAr){
  int i, c, cen = 0;
  //random selection of centroids from input file
  //random_centroid_array holds their ids
  int* random_centroid_array = malloc (pinfo->clusters*sizeof(int));
  if(random_centroid_array == NULL){
    printf("Error! memory not allocated.");
    return;
  }
  randomCentroids(random_centroid_array, pinfo->numCurves, pinfo->clusters);  //uniform rand
  //save to centroidAr
  for(i = 0; i < pinfo->numCurves; i++){
    for(c = 0; c < pinfo->clusters; c++){
      if(curveAr[i]->id == random_centroid_array[c]){
        centroidAr[cen] = curveAr[i];
        centroidAr[cen]->is_centroid = 1;
        cen++;
      }
    }
  }
  // for(c = 0; c < pinfo->clusters; c++){
  //   printf("CENTROID AR %d\n", centroidAr[c]->id);
  // }
  free(random_centroid_array);
}

void randomCentroids(int* random_centroid_array, int numCurves, int centroids){
  int i, j;
  for(i = 0; i < centroids; i++){
    random_centroid_array[i] = 0 + (rand()/(RAND_MAX+1.0))*(numCurves-1-0+1);
    j = 0;
    while(j < i){
      // printf("in for: i is %d\n",i );
       if(random_centroid_array[j] == random_centroid_array[i]){
         while((random_centroid_array[j] == random_centroid_array[i])){
           random_centroid_array[i] = 0+(rand()/(RAND_MAX+1.0))*(numCurves-1-0+1);
           j = 0;
         }
       }
       else j++;
     }
  }
}

void kmeans(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo){

  int t = 1; //we start with one centroid
  int M = 0;
  int uniformly_random_centroid, i, j, r, d, k;

  int D = pinfo->D;
  int clusters = pinfo->clusters;
  int numCurves = pinfo->numCurves;

  int random_centroids[clusters];

  long double Di[clusters][numCurves], P[numCurves], p = 0.0;
  long double dist, min = 0.0, max;
  double x;

  int* new_centers = malloc(clusters * sizeof(int));
  if(new_centers == NULL){
        printf("Error! memory not allocated.");
        return;
  }
  for(i = 0; i < clusters; i++){
    new_centers[i] = -1;
  }

  while(t <= clusters){
    uniformly_random_centroid = M + (rand()/(RAND_MAX+1.0))*((numCurves-1)-M+1);
    // printf("random centroid %d\n", uniformly_random_centroid);
    random_centroids[t-1] = uniformly_random_centroid;

    for(i = 0; i < numCurves; i++){ //for each curve
      for(j = 0; j < t; j++){
        if(i != random_centroids[j]){//non centroid
          //find distance of non centroid curve with all uniformly_random_centroids
          if(pinfo->f == 1) dist = DFD_function(curveAr[i], curveAr[random_centroids[j]], D);
          else if(pinfo->f == 2) dist = DTW_function(curveAr[i], curveAr[random_centroids[j]], D);

          //keep min
          if(min == 0.0)  min = dist;
          else if(dist < min) min = dist;

          //save
          Di[t-1][i] = min;
        }
        else{
          Di[t-1][i] = 0.0;
          break;
        }
      }
      min = 0.0;
    }

    //partial sums
    for(i = 0; i < numCurves; i++){
      P[i] = Di[t-1][i] + p;
      // printf("%Lf + %Lf = %Lf\n", Di[t-1][i], p, P[i]);
      p = P[i];
      // printf("p %Lf\n", p);
    }

    // for(i = 0; i < numCurves; i++){
    //   printf("P[%d] = %Lf\n", i, P[i] );
    // }

    //Pick a uniformly distributed float x ∈ [0, P(n)]

    max = P[numCurves - 1];
    x = rand()/(RAND_MAX+1.0)*max;

    // printf("x = %f\n", x);

    //choose r
    for(i = 0; i < numCurves; i++){
      if(i == 0){
        if(x <= P[i]){
          r = i;
        }
      }
      else{
        if(x > P[i-1] && x <= P[i]){
  				r = i;//new center
  			}
      }
		}
    // printf("new center is %d\n", r);
    //r chosen


    //check if duplicate
    k = 0;
    while(k < t-1){
      // printf("is %d == %d?\n", new_centers[k], r);
      if(new_centers[k] == r){
        while(new_centers[k] == r){
          max = P[numCurves - 1];
          x = rand()/(RAND_MAX+1.0)*max;

          // printf("x = %f\n", x);

          //choose r
          for(i = 0; i < numCurves; i++){
            if(i == 0){
              if(x <= P[i]){
                r = i;
              }
            }
            else{
              if(x > P[i-1] && x <= P[i]){
        				r = i;//new center
        			}
            }
      		}
          // printf("new center is %d\n", r);
          k = 0;
          //r chosen
        }

      }
      else  k++;
    }

    new_centers[t-1] = r;
    p = 0.0;
    t++;
  }

  //sort new_centers
  int c, swap;

  for (c = 0 ; c < ( clusters - 1 ); c++){
    for (d = 0 ; d < clusters - c - 1; d++){
      if (new_centers[d] > new_centers[d+1])
      {
        swap       = new_centers[d];
        new_centers[d]   = new_centers[d+1];
        new_centers[d+1] = swap;
      }
    }
  }

  //save to centroidAr
  for(i = 0; i < numCurves; i++){
    for(j = 0; j < clusters; j++){
      if(i == new_centers[j]){
        centroidAr[j] = curveAr[i];
        centroidAr[j]->is_centroid = 1;
      }
    }
  }


  free(new_centers);
}
