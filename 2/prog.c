#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "prog.h"
#include "grid.h"

void printHelp(char *progName) {
    printf("Usage: %s arguments\n\
\033[22;33m-i\033[m [filename]  input file\n\
\033[22;33m-c\033[m [filename]  configuration file\n\
\033[22;33m-o\033[m  [filename] output file\n\
\033[22;33m-d\033[m  [DFD or DTW]\n", progName);

}

int argHandling(Pinfo pinfo, int argc, char **argv) {
    int argIter = 1, f = 0;
    char input[20], fd[3], output[20], conf[15];
    char* dfd, dtw;

    //initialize pinfo
    memset(pinfo->fd,'\0',sizeof(pinfo->fd));
    memset(pinfo->input,'\0',sizeof(pinfo->input));
    memset(pinfo->conf,'\0',sizeof(pinfo->conf));
    memset(pinfo->output,'\0',sizeof(pinfo->output));
    pinfo->f = 0;

    memset(fd,'\0',sizeof(fd));
    memset(input,'\0',sizeof(input));
    memset(conf,'\0',sizeof(conf));
    memset(output,'\0',sizeof(output));

    if(argc == 1) return EXIT_FAILURE;
    // printf("%d\n",argc );

    while (argIter < argc-1) {
        if (argv[argIter][0] == '-') {
            if (argv[argIter][2] == '\0') {
                switch (argv[argIter][1]) {
                  case 'i':
                       argIter++;
                       strcpy(input, argv[argIter]);
                      //  printf("hello %s\n",input);
                       break;
                  case 'c':
                       argIter++;
                       strcpy(conf, argv[argIter]);
                      //  printf("hello %s\n", conf);
                       break;
                 case 'o':
                      argIter++;
                      strcpy(output, argv[argIter]);
                      // printf("hello %s\n", output);
                      break;
                  case 'd':
                      argIter++;
                      strcpy(fd, argv[argIter]);
                      if(strcmp(fd, "DFD") == 0){
                          f = 1;
                      }
                      else if(strcmp(fd, "DTW") == 0){
                          f = 2;
                      }
                      // printf("hello %s\n", fd);
                      break;
                  default:
                      // printf("1\n" );
                      return EXIT_FAILURE;
              }
          }
          else{
            // printf("2\n" );
            return EXIT_FAILURE;
          }
      }
      else{
        // printf("3\n" );
        return EXIT_FAILURE;
      }
      ++argIter;
    }

    memcpy(pinfo->fd, fd, sizeof(fd));
    // printf("fd %s\n", pinfo->fd);

    pinfo->f = f;
    // printf("f %d\n", pinfo->f);

    memcpy(pinfo->input, input, sizeof(input));
    // printf("input %s\n", pinfo->input);

    memcpy(pinfo->output, output, sizeof(output));
    // printf("output %s\n", pinfo->output);

    memcpy(pinfo->conf, conf, sizeof(conf));
    // printf("conf %s\n", pinfo->conf);



    if(pinfo->f == 0 || strlen(pinfo->input) == 0 || strlen(pinfo->output) == 0 || strlen(pinfo->conf) == 0){
      // printf("f %d\n", pinfo->f);
      // printf("%s\n", pinfo->input);
      // printf("%s\n", pinfo->output);
      // printf("%s\n", pinfo->conf);
      // printf("4\n" );
      return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void confHandling(Pinfo pinfo){
  FILE *conf;
  conf = fopen(pinfo->conf,"r");    //configuration file

  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int temp_ar[3]={0,0,0};
  int ep=0;
  while((read = getline(&line, &len, conf)) != -1){
    line[read - 1] = 0;
    char *pch;
    pch = strtok(line, " ");
    pch = strtok(NULL, " ");
    if(pch == "-"){
      ep++;
      continue;
    }else{
      temp_ar[ep] = atoi(pch);
      ep++;
    }
  }
  if(temp_ar[0]>0){
    pinfo->clusters = temp_ar[0];
  }
  if(temp_ar[1]>0){
    pinfo->K = temp_ar[1];
  }
  else{
    pinfo->K = 2;
  }
  if(temp_ar[2]>0){
    pinfo->L = temp_ar[2];
  }
  else{
    pinfo->L = 3;
  }

  printf("centroids: %d\n",pinfo->clusters);
  printf("K %d\n",pinfo->K );
  printf("L %d\n",pinfo->L );

  fclose(conf);
  free(line);
}

void firstRead(Pinfo pinfo, FILE *outf){
  int numCurves, points, D;
  int min = 0,  max = 0, fl = 0;
  long double delta;

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  stream = fopen(pinfo->input, "r" );

  while ((read = getline(&line, &len, stream)) != -1) {
    line[read - 1] = 0;
    char *pch;

    if(fl == 0){
      pch = strtok(line, " ");
      pch = strtok(NULL, " ");
      D = atoi(pch);
      fl = 1;
      continue;
    }

    pch = strtok(line, "\t");
    numCurves = atoi(pch) + 1;  //id starts with 0

    pch = strtok(NULL, "\t");
    pch[strlen(pch)] = 0;
    points = atoi(pch);
    if(min == 0){
      min = points;
    }
    else{
      if(points < min)  min = points;     //min points
    }

    if(points > max)  max = points;       //max points
  }

  delta = (4*D*min);

  pinfo->D = D;
  pinfo->delta = delta;
  pinfo->numCurves = numCurves;
  pinfo->maxPoints = max;
  pinfo->minPoints = min;

  fprintf(outf,"Curves are %d\n", pinfo->numCurves);
  // fprintf(outf,"max points are %d\n", pinfo->maxPoints);
  // fprintf(outf,"min points are %d\n", pinfo->minPoints);
  // fprintf(outf,"DELTA = %Lf\n", pinfo->delta);

  fclose(stream);
  free(line);
}

void secondRead(Pinfo pinfo, curvePtr* curveAr){
  int i, j, flag;
  int duplicates = 0, position = 0, fl = 0;
  long double x, y ,temp;
  int D = pinfo->D;

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  stream = fopen(pinfo->input, "r" );
  while ((read = getline(&line, &len, stream)) != -1) {
      line[read - 1] = 0;
      char *pch;

      if(fl == 0){
        fl = 1;
        continue;
      }

      curvePtr curveP = malloc(sizeof(Curve));
      if(curveP == NULL){
            printf("Error! memory not allocated.");
            return;
      }

      pch = strtok(line, "\t");
      curveP->id = atoi(pch);

      curveP->is_centroid = -1;
      curveP->has_centroid = -1;
      curveP->cenBuckets = NULL;
      curveP->candidate = -1;

      pch = strtok(NULL, "\t");
      pch[strlen(pch)] = 0;
      curveP->points = atoi(pch);

      curveP->coordinates = (long double*)malloc(D * curveP->points * sizeof(long double));
      if(curveP->coordinates == NULL) {
            printf("Error! memory not allocated.");
            return;
      }

      for(i = 0; i < curveP->points*D; i++){
        if(i <= 1){
          pch = strtok(NULL, ",");
          pch[strlen(pch) - 1] = 0;
          pch++;
          curveP->coordinates[i] = strtold(pch, NULL);

          i++;

          pch = strtok(NULL, ",");
          pch[strlen(pch) - 1] = 0;
          pch++;
          curveP->coordinates[i] = strtold(pch, NULL);
          j=2;
        }
        else{
          flag=0;
          pch = strtok(NULL, ",");
          pch[strlen(pch) - 1] = 0;
          pch += 2;
          temp = strtold(pch, NULL);
          if(curveP->coordinates[j-2] == temp){
            flag++;
          }
          i++;

          pch = strtok(NULL, ",");
          pch[strlen(pch) - 1] = 0;
          pch++;
          if((curveP->coordinates[j-1] == strtold(pch, NULL)) && flag==1){
            flag++;
            //fprintf(outf,"x dup %.15Lf y dup %.15Lf \n",temp,curveP->coordinates[j] = strtold(pch, NULL));
          }else{
            //fprintf(outf,"easy pass\n");
            curveP->coordinates[j]=temp;
            curveP->coordinates[j+1] = strtold(pch, NULL);
            j+=2;
            //fprintf(outf,"x %.15Lf y %.15Lf \n",curveP->coordinates[j-2],curveP->coordinates[j-1] = strtold(pch, NULL));
          }
          if(flag == D){
            //fprintf(outf," dups are %.15Lf %.15Lf %.15Lf %.15Lf \n",curveP->coordinates[i-1],curveP->coordinates[i],curveP->coordinates[i-3],curveP->coordinates[i-2]);
            duplicates++;
          }
        }
      }
      curveP->points=curveP->points-duplicates;
      // fprintf(outf,"New points %d\n",curveP->points);
      // fprintf(outf,"dups %d\n",duplicates);
      curveP->coordinates = (long double*)realloc(curveP->coordinates ,D * curveP->points * sizeof(long double));
      duplicates = 0;

      //finished reading curve
      curveAr[position] = curveP;   //saved to array
      position++;
  }
  fclose(stream);
  free(line);
}
