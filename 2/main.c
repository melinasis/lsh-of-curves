#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "prog.h"
#include "grid.h"
#include "hashTable.h"
#include "cluster.h"

int main(int argc, char *argv[]){

  char str1[3];
  int A, B, C;
  int c, l, i, j, k;
  nodePtr g = NULL;
  FILE *outf;

  int** centroidL;
  int* ar_ri;
  curvePtr* candidates;

  long double obj_fun = -1.0, new_obj_fun;
  int stop_update = 0, loop = 0;

  Pinfo pinfo = malloc(sizeof(Prog));
  if(pinfo == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }

  if (argHandling(pinfo, argc, argv) == EXIT_FAILURE){
  		printHelp(argv[0]);
  		return EXIT_FAILURE;
  }

  confHandling(pinfo);  //read from configuration

  outf = fopen(pinfo->output,"w");  //output file
  fprintf(outf,"HashFunction: Propabilistic\n");
  if(pinfo->f == 1) fprintf(outf,"Distance Function: DFD\n" );
  else if(pinfo->f == 2)  fprintf(outf,"Distance Function: DTW\n" );

  firstRead(pinfo, outf); //first read of input file
  //finds D,delta, numCurves, minPoints, maxPoints

  int K = pinfo->K;
  int L = pinfo->L;
  int D = pinfo->D;
  int clusters = pinfo->clusters;
  int numCurves = pinfo->numCurves;
  int maxPoints = pinfo->maxPoints;

  //curveAr holds all curves from input
  curvePtr* curveAr = malloc(numCurves*sizeof(curvePtr));
  if(curveAr == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }

  //centroidAr holds all centroid curves
  curvePtr* centroidAr = malloc(clusters*sizeof(curvePtr));
  if(centroidAr == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }

  //LSH_Ar holds all HashTables for non centroid curves
  BucketPtr** LSH_Ar = malloc(L * sizeof(BucketPtr*));
  if(LSH_Ar == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }
  for(l = 0; l < L; l++){LSH_Ar[l] = createHashTable(numCurves);}


  secondRead(pinfo, curveAr);//first read of input file
  //stores input to curveAr, removes duplicate points

  printf("Enter combination: ");
  scanf("%s", str1);
  if((str1[2] == '1') && (pinfo->f == 2)){
      printf("Update 1 cannot be called for DTW\n");
      return EXIT_FAILURE;
  }
  if(str1[0] == '1'){A = 1;}
  else if(str1[0] == '2'){A = 2;}
  if(str1[1] == '1'){B = 1;}
  else if(str1[1] == '2'){B = 2;}
  if(str1[2] == '1'){C = 1;}
  else if(str1[2] == '2'){C = 2;}

                     //INITIALIZATION

   srand(time(NULL));
   clock_t begin = clock();
  //a. random
  if(A == 1){
    fprintf(outf,"\nINITIALIZATION: Random\n");
    randomInit(pinfo, curveAr, centroidAr);
  }
  //b. KMEANS
  else if(A == 2){
    fprintf(outf,"\nINITIALIZATION: K-means\n");
    kmeans(curveAr, centroidAr, pinfo);
  }

  // for(j = 0; j < clusters; j++){
  //   printf("%d\n", centroidAr[j]->id);
  // }


                        //ASSIGNMENT
while(stop_update == 0){
  //a. lloyds assignment
  if(B == 1){
    fprintf(outf,"ASSIGNMENT: Lloyd's\n");
    lloyds(curveAr, centroidAr, pinfo);
  }

  //b. assignment by Range Search
  else if(B == 2){
    fprintf(outf,"ASSIGNMENT: LSH Range Search\n");

    if(loop == 0){
      //keep number of centroids of each bucket in all (L) HashTables
      centroidL = malloc(L * sizeof(int*));
      if(centroidL == NULL){
            printf("Error! memory not allocated.");
            return EXIT_FAILURE;
      }
      for(l = 0; l < L; l++){
        centroidL[l] = malloc(numCurves/8 * sizeof(int));
        if(centroidL[l] == NULL){
              printf("Error! memory not allocated.");
              return EXIT_FAILURE;
        }
      }

      //ar_ri
      ar_ri = malloc(K*maxPoints*D * sizeof(int));
      if(ar_ri == NULL){
            printf("Error! memory not allocated.");
            return EXIT_FAILURE;
      }
      ri(ar_ri, K, maxPoints, D);
    }
    assignmentRangeSearch(ar_ri, loop, LSH_Ar, centroidAr, curveAr, pinfo, centroidL);
  }

  // for(c = 0; c < clusters; c++){
  //   printf("\033[22;31m%d\033[m: ",centroidAr[c]->id );
  //   for(i = 0; i < numCurves; i++){
  //     if(curveAr[i]->has_centroid == centroidAr[c]->id){
  //       printf("%d ", curveAr[i]->id);
  //     }
  //   }
  //   printf("\n" );
  // }printf("\n");

                          //END OF ASSIGNMENT

  for(i = 0; i < numCurves; i++){
    if(curveAr[i]->has_centroid != -1){
      curveAr[i]->candidate = curveAr[i]->has_centroid;
    }
  }

                        //UPDATE
  if(loop == 0){
    candidates = malloc(clusters * sizeof(curvePtr));
  }
  for(c = 0; c < clusters; c++){
    candidates[c] = centroidAr[c];
  }

  //a. Mean Discrete Frechet Curve
  if(C == 1){
    fprintf(outf,"UPDATE: Mean Discrete Frechet Curve\n\n");
    for(c = 0; c < clusters; c++){  //update each cluster(centroid)
      curvePtr updatedCenter = malloc(sizeof(Curve));
      updateMDFC(loop, curveAr, centroidAr[c], pinfo, c, updatedCenter);

      if(updatedCenter->id != -1 && updatedCenter->id >= numCurves){
        // printf("updatedCenter %d\n", updatedCenter->id);
        for(i = 0; i < numCurves; i++){
          if(curveAr[i]->has_centroid == centroidAr[c]->id){
            // printf("%d is centroid\n", curveAr[i]->id);
            curveAr[i]->candidate = updatedCenter->id;
          }
        }
        candidates[c] = updatedCenter;
      }
      else{
        free(updatedCenter->coordinates);
        free(updatedCenter);
      }
    }
    //we have an updated array of centroids
    //check if we keep it


                          //OBJECTIVE FUNCTION
    new_obj_fun = ObjectiveFunctionA(curveAr, candidates, pinfo);
    // printf("new obj fun is %Lf\n", new_obj_fun);

    if(obj_fun == -1){
      obj_fun = new_obj_fun;
      stop_update = 0;
      loop = loop + 1;;
    }
    if(obj_fun > new_obj_fun){
      obj_fun = new_obj_fun;
      stop_update = 0;
      loop = loop + 1;
    }
    else if(obj_fun < new_obj_fun){
      stop_update = 1;
      // printf("STOP LOOP\n" );
      for(c = 0; c < clusters; c++){
        if(candidates[c]->id != centroidAr[c]->id){
          free(candidates[c]->coordinates);
          free(candidates[c]);
        }
      }
    }

    if(stop_update == 0){
      //we keep it
      for(c = 0; c < clusters; c++){
        if(candidates[c]->id != centroidAr[c]->id){
          if(centroidAr[c]->id >= numCurves){
            free(centroidAr[c]->coordinates);
            free(centroidAr[c]);
            // printf("free updated center!\n" );
          }
          centroidAr[c] = candidates[c];
        }
      }


      //initialize all Curves to have no centroids
      for(i = 0; i < numCurves; i++){
        if(curveAr[i]->is_centroid == -1){
          curveAr[i]->has_centroid = -1;
          curveAr[i]->candidate = -1;
          curveAr[i]->cenBuckets = NULL;
        }
      }

    }

    // for(c = 0; c < clusters; c++){
    //   printf("\033[22;31m%d\033[m: ",centroidAr[c]->id );
    //   for(i = 0; i < numCurves; i++){
    //     if(curveAr[i]->has_centroid == centroidAr[c]->id){
    //       printf("%d ", curveAr[i]->id);
    //     }
    //   }
    //   printf("\n" );
    // }printf("\n");

    for(c = 0; c < clusters; c++){
      candidates[c] = NULL;
    }
  }
  else if(C == 2){
    fprintf(outf,"UPDATE: PAM\n\n");
    curvePtr new;
    new = updatePAM(curveAr, centroidAr, pinfo);

    if(new != NULL){
      // printf("NEW %d\n",new->id );
      if(new->has_centroid != -1){
        for(c = 0; c < clusters; c++){
          if(centroidAr[c]->id == new->has_centroid){
            centroidAr[c]->is_centroid = -1;
            new->has_centroid = -1;
            new->is_centroid = 1;
            if(B == 2){
              new->cenBuckets = malloc(L * sizeof(int));
              if(new->cenBuckets == NULL){
                    printf("Error! memory not allocated.");
                    return EXIT_FAILURE;
              }
              for(l = 0; l < L; l++){
                new->cenBuckets[l] = findBucket(LSH_Ar[l], new->id, numCurves);
                // printf("in bucket %d\n", new->cenBuckets[l]);
              }
            }
            centroidAr[c] = new;
          }
        }
        //initialize all Curves to have no centroids
        for(i = 0; i < numCurves; i++){
          if(curveAr[i]->is_centroid == -1){
            curveAr[i]->has_centroid = -1;
          }
        }
        loop = loop + 1;
      }
      else  stop_update = 1;
    }

    // for(c = 0; c < clusters; c++){
    //   printf("\033[22;31m%d\033[m: ",centroidAr[c]->id );
    //   for(i = 0; i < numCurves; i++){
    //     if(curveAr[i]->has_centroid == centroidAr[c]->id){
    //       printf("%d ", curveAr[i]->id);
    //     }
    //   }
    //   printf("\n" );
    // }printf("\n");
  }
}

                              //OUTPUT FILE
  int sum = 0;
  for(c = 0; c < clusters; c++){
  fprintf(outf, "CLUSTER %d {", c);
  if(centroidAr[c]->id >= numCurves){
  fprintf(outf, "New center coordinates: " );
  for(j = 0; j < centroidAr[c]->points; j++){
  fprintf(outf, "%Lf ", centroidAr[c]->coordinates[j]);
  }
  fprintf(outf, "\n");
  }
  fprintf(outf, "centroid: %d (",centroidAr[c]->id );
  for(i = 0; i < numCurves; i++){
  if(curveAr[i]->has_centroid == centroidAr[c]->id){
  fprintf(outf, "%d ", curveAr[i]->id);
  sum = sum + 1;
  }
  }
  fprintf(outf, ")}\tsize: %d\n\n", sum);
  sum = 0;
  }
  clock_t end = clock();
  double time_spent = (double)(end-begin)/CLOCKS_PER_SEC;
  fprintf(outf,"clustering time: %f\n", time_spent);

                            //SILHOUETTE
  silhouette(pinfo, curveAr, centroidAr, outf);

                            //FREE
  for(l = 0; l < L; l++){
    freeHashTable(LSH_Ar[l], numCurves, l);
  }
  free(LSH_Ar);

  for(c = 0; c < clusters; c++){
    if(centroidAr[c]->id >= numCurves){
      if(centroidAr[c]->cenBuckets != NULL)
        free(centroidAr[c]->cenBuckets);
      free(centroidAr[c]->coordinates);
      free(centroidAr[c]);
      // printf("FREE UPDATED CENTER\n" );
    }
  }
  free(centroidAr);

  for(i = 0; i < numCurves; i++){

    if(curveAr[i]->cenBuckets != NULL){
      free(curveAr[i]->cenBuckets);
    }

    free(curveAr[i]->coordinates);
    free(curveAr[i]);
  }
  if(B == 2){
    for(i = 0; i < L; i++){
      free(centroidL[i]);
    }
    free(centroidL);
    free(ar_ri);
  }
  free(curveAr);
  free(candidates);
  free(pinfo);
  fclose(outf);

  return 0;
}
