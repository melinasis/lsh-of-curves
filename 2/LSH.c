#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "prog.h"
#include "grid.h"
#include "hashTable.h"

void ri(int* ar_ri, int K, int maxPoints, int D){
  int myRand, range, i;
  int rangeLow=0;
  int rangeHigh=10000;
  for(i=0;i<K*maxPoints*D;i++){
    myRand = (int)rand();
    range = rangeHigh - rangeLow + 1;
    ar_ri[i] = (myRand % range) + rangeLow;
  }
}
void concatenate_lsh(nodePtr head, nodePtr newList){
  nodePtr current = head;
  while (current->next != NULL) {
      current = current->next;
  }
    current->next = newList;
}

nodePtr createHlist(){
  nodePtr h = malloc(sizeof(node));
  if(h == NULL)
  {
        printf("Error! memory not allocated.");
        EXIT_FAILURE;
  }
  h->point = 0.0;
  h->next = NULL;
  return h;
}

void push_h(nodePtr head, long double p) {
    nodePtr current = head;
    if(current->point == 0.0){    //first node
      current->point = p;
    }
    else{
      while (current->next != NULL) {
          current = current->next;
      }
      current->next = malloc(sizeof(node));
      if(current->next == NULL)
      {
            printf("Error! memory not allocated.");
            EXIT_FAILURE;
      }
      current->next->point = p;
      current->next->next = NULL;
    }
}

void marsaglia(long double* v, int D){
  double lowerRange = 0.0;
  double upperRange = 1.0;
  int i;
  long double l[D],sq[D],y[D],x[D];
  long double r = 0.0;


  for(i=0;i<D;i++){
    y[i] = ((double)rand() * (upperRange - lowerRange)) / (double)RAND_MAX + lowerRange;
    // printf("y%d %Lf\n",i, y[i]);
  }

  for(i = 0; i < D; i++){
    r+= y[i] * y[i];
  }
  // printf("r=%Lf\n",r);


  for(i = 0;i < D; i++){
    l[i] = log(y[i]);
    // printf("log=%Lf\n",l[i]);
    sq[i] = sqrt(-2 * l[i] / 2);
    // printf("sqrt=%Lf\n",sq[i]);
    // printf("x%d = y%d * sp%d\n",i,i,i );
    x[i] = y[i] * sq[i];
  }//end marsaglia method
  for(i = 0; i < D; i++){
    v[i] = x[i];
  }
  return;
}

double t_calc(int w){

  int fMax = w;
  int fMin = 0;
  double t1 = (double)rand() / RAND_MAX;//t in range[0,w)
  double t = fMin + t1 * (fMax - fMin);
  // printf("random t=%lf\n",t);
}

void free_amplified(nodePtr head){
  nodePtr temp;
  nodePtr current = head;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
  // printf("free amplified\n");
}

int LSH_keyValue(nodePtr g, int numCurves, int ar_ri[], int D){
  int w = 16, j, num, tableSize, ri = 0, i , xi = 0;
  int f = 0;
  long long a = 0, M = 0;
  double t;
  nodePtr h, lsh_g;
  long double p, pv, new;
  long double* v;

  v = calloc(D, sizeof(long double));
  num = countNode(g);
  // printf("NUMBER OF NODES: %d\n", num);
  nodePtr current = g;

  for(j = 0; j < KVEC; j++){  //kvec * h
    h = createHlist();

    marsaglia(v, D);
    t = t_calc(w);
    for(i = 0; i < num; i++){

        p = current->point;
        if(i%2 == 0){   //x
          pv = p * v[0];
        }
        else{   //y
          pv = p * v[1];
        }
  //       // printf("pv %Lf\n", pv);
        new = (pv + t) / w;
  //       // printf("new %Lf\n", new);
        push_h(h, new);
        current = current->next;
    }
    if(j > 0){//a list is already created
      concatenate_lsh(lsh_g, h);
    }
    else{
      lsh_g = h;
    }
    current = g;
  }//lsh_g amplified


  M = pow(2, 32) - 5;
  // printf("M %lld\n", M);

  tableSize = numCurves / 8;
  // printf("tableSize %d\n", tableSize);

  num = countNode(lsh_g);

  current = lsh_g;
  for(i = 0; i < num; i++){
    xi = (int)current->point;
    ri = ar_ri[i];

    a = (xi*ri) % M;
    f += a;

    current = current->next;
  }
  if(f < 0){
    f = -f;
  }

  f = f % tableSize;

  free(v);
  free_amplified(lsh_g);

  return f;
}

void LSH_Hash(nodePtr g, int numCurves, BucketPtr* LSH_HashTable, curvePtr curveP, int ar_ri[], int D, int l){
  int key = LSH_keyValue(g, numCurves, ar_ri, D);
  //keep number of bucket(key) that centroids hash into
  if(curveP->is_centroid == 1){
    curveP->cenBuckets[l] = key;
  }
  // printf("KEY is %d\n", key);
  insertHashTable(LSH_HashTable, key, curveP, g);
}

int findBucket(BucketPtr* HashTable, int id, int numCurves){
  int bucket, i;
  BucketPtr current;

  for(i = 0; i < numCurves/8; i++){
    current = HashTable[i];
    while(current != NULL){
      if(current->id == id){
        bucket = i;
        break;
      }
      else  current = current->next;
    }
  }
  return bucket;
}
void centroid_flag(BucketPtr* HashTable, int numCurves, curvePtr curveP, int l){
  int bucket;
  BucketPtr current;

  bucket = curveP->cenBuckets[l];
  current = HashTable[bucket];
  while(current->id != curveP->id){
    current = current->next;
  }
  current->is_centroid = 1;
}

void moveCentroidsStart(BucketPtr* HashTable, Pinfo pinfo, curvePtr* centroidAr, int l){
  int c, bucket;
  BucketPtr prev, current;

  for(c = 0; c < pinfo->clusters; c++){
    bucket = centroidAr[c]->cenBuckets[l];
    current = HashTable[bucket];
    prev = NULL;
    while(current->id != centroidAr[c]->id){
      prev = current;
      current = current->next;
    }
    if(prev != NULL){
      prev->next = current->next;
      current->next = HashTable[bucket];
      HashTable[bucket] = current;
    }
  }
}
