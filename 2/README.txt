Ανάπτυξη Λογισμικού για Αλγοριθμικά Προβλήματα 2017
Μέρος Β

Μελίνα Σίσκου 1115201200161
Γεώργιος Ρούσσος - Μπάλλας 1115201300151

Για compile εντολή make.
Γραμμή εντολής ./proj -i inputfile -d [DFD or DTW] -o outputfile -c conf

Input file:	πρώτη γραμμή D= [dimension]
Conf file:	number_of_clusters: 
		number_of_grid_curves: [- for default]
		number_of_hash_tables: [- for default]

Το πρόγραμμα δέχεται από τον χρήστη τον τρόπο εκτέλεσης
Enter combination: [initialization][assignment][update]
Αν έχει δωθεί DTW, δεν μπορεί να υλοποιηθεί το update 1 (MDFC)

Κατάλογος αρχείων:
main.c		prog.h
prog.c		grid.h
grid.c		hashTable.h
classicHash.c	cluster.h
LSH.c		Makefile
initialization.c
assignment.c
update.c
distances.c
silhouette.c

INITIALIZATION
Πίνακας centroidAr μεγέθους όσα τα κέντρα, με δείκτες στις καμπύλες που επιλέχθηκαν ως κέντρα 
Μεταβλητή is_centroid = 1 για τα κέντρα

ASSIGNMENT
Μεταβλητή has_centroid = id καμπύλης κέντρου στο οποίο ανήκει

για το LSH Range Search:
Την πρώτη φορά (loop = 0) όλα τα σημεία (κεντροειδή και μη) περνάνε από το grid και αποθηκεύονται σε L hashtables.
Επαναληπτικά (loop > 0), μόνο αν έχουν προκύψει νέες καμπύλες από το MDFC Update περνάνε και αυτές από το grid και αποθηκεύονται στους ήδη υπάρχοντες πίνακες κατακερματισμού.
Σε κάθε περίπτωση οι καμπύλες κέντρα έχουν τον πίνακα cenBuckets που διατηρεί σε ποιο bucket βρίσκονται σε κάθε ένα από τα L hashtables.
Για διευκόλυνση του αλγορίθμου τα κέντρα μετακινούνται στην αρχή του bucket που ανήκουν.(moveCentroidsStart()).
Για ακτίνα R κάθε κέντρο "μαζεύει" καμπύλες, ελέγχοντας το bucket στο οποίο ανήκει σε όλους τους πίνακες κατακερματισμού.(rangeSearchBucket()). Τυχόν καμπύλες που δεν έχουν ανατεθεί, συγκρίνονται με όλα τα κέντρα (checkUnassigned()).
Μεταβλητές των hashtables (is_centroid, has_centroid) γίνονται reset ώστε να επαναχρησιμοποιηθούν αν χρειαστεί μετά το update.(resetHashTable()).

UPDATE

για το Mean Discrete Frechet Curve:
Δημιουργούμε προσωρινό πίνακα candidates για τα κέντρα που θα προκύψουν. 
Όλες οι καμπύλες που ανήκουν σε ένα cluster μπαίνουν σε μια συνδεδεμένη λίστα. Γνωρίζοντας τον αριθμό τους μπορούμε να βρούμε το επιθυμητό ύψος του δυαδικού δέντρου.
Φτιάχνουμε κενό δυαδικό δέντρο και ως φύλλα βάζουμε τις τιμές της λίστας. Υπολογίζουμε τις μέσες καμπύλες φτάνοντας στην ζητούμενη της ρίζας.
Ο πίνακας candidates έχει την νέα εικόνα των κέντρων. Εξετάζεται αν αυτή προσφέρει καλύτερο στόχο και αποφασίζεται αν θα συνεχιστεί ο επαναληπτικός βρόγχος ή όχι (stop_update = 1).
Αν συνεχιστεί ο πίνακας centroidAr παίρνει τις τιμές του candidates και οι μεταβλητές is_centroid, has_centroid τροποποιούνται καταλλήλως.

για τον PAM:
κάθε update του PAM  επιστρέφει ένα μόνο καλύτερο κέντρο που ανήκει στο αρχικό dataset.

VALGRIND: No errors, no memory leaks





