#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "prog.h"
#include "grid.h"
#include "hashTable.h"

long double max(long double d1,long double d2){
  if (d1>d2){
    return d1;
  }else{
    return d2;
  }
}

long double min(long double d1,long double d2,long double d3){
  if(d2<=d1 && d2<=d3 ){
    return d2;
  }else if(d1<=d2 && d1<=d3){
    return d1;
  }else if(d3<=d1 && d3<=d2){
    return d3;
  }
}

long double coor_dist(int D,int i,int j,long double *p,long double *q){
  fflush(stdout);
  long double diff,power,sqr,sum=0;
  int k;
    for(k=0;k<D;k++){
      //printf("p:%Lf-q:%Lf\n",p[i+k],q[j+k]);
      diff=(p[i+k])-(q[j+k]);
      //printf("diff=%Lf\n",diff);
      power=pow(diff,2);
      //printf("pow=%Lf\n",power);
      sum =sum+power;
    }
    sqr=sqrt(sum);
    //printf("sqr=%Lf\n",sqr);
  return sqr;
}

long double DFD_function(curvePtr p,curvePtr q,int D){

  long double maxd, mind ,d;
  int m = q->points;
  int n = p->points;

  long double L_ar[n][m];
  int i,j;

  for(i = 0; i < n; i++){
    for(j = 0; j < m; j++){
      if(i == 0 && j == 0){
        L_ar[i][j]=coor_dist(D,i,j,p->coordinates,q->coordinates);
        //L_ar[i][j]=dist(p->coordinates,q->coordinates,D);
        // printf("0,0=%Lf\n",L_ar[i][j]);
      }else if (i==0){
        d=coor_dist(D,i,j,p->coordinates,q->coordinates);
        //d=dist(p->coordinates,q->coordinates,D);
        maxd=max(d,L_ar[i][j-1]);
        L_ar[i][j]=maxd;
        // printf("0,%d=%Lf\n",j,L_ar[i][j]);
      }else if(j==0){
        d=coor_dist(D,i,j,p->coordinates,q->coordinates);
        //d=dist(p->coordinates,q->coordinates,D);
        maxd=max(d,L_ar[i-1][j]);
        L_ar[i][j]=maxd;
        // printf("%d,0=%Lf\n",i,L_ar[i][j]);
      }else{
        d=coor_dist(D,i,j,p->coordinates,q->coordinates);
        //d=dist(p->coordinates,q->coordinates,D);
        mind=min(L_ar[i-1][j],L_ar[i][j-1],L_ar[i-1][j-1]);
        maxd=max(d,mind);
        L_ar[i][j]=maxd;
        // printf("%d,%d=%Lf\n",i,j,L_ar[i][j]);
      }
    }
  }
  // printf("final table=%Lf\n",L_ar[n-1][m-1]);
  return L_ar[n-1][m-1];
}

long double DTW_function(curvePtr p,curvePtr q,int D){
  int i,j,m1,m2;
  long double mind,d;
  m1=p->points;
  m2=q->points;
  long double C_ar[m1][m2];
  for(i=0;i<m1;i++){
    C_ar[i][0]=1029;
    //printf(" %d,0=%Lf\n",i,C_ar[i][0]);
  }
  for(i=0;i<m2;i++){
    C_ar[0][i]=1209;
    //printf(" 0,%d=%Lf\n",i,C_ar[0][j]);
  }
  C_ar[0][0]=0;
  //printf(" 0,0=%Lf\n",C_ar[i][j]);
  for(i=1;i<m1;i++){
    for(j=1;j<m2;j++){
      mind=min(C_ar[i-1][j],C_ar[i][j-1],C_ar[i-1][j-1]);
      //d=dist(p->coordinates,q->coordinates,D);
      d=coor_dist(D,i,j,p->coordinates,q->coordinates);
      C_ar[i][j]=d+mind;
      //printf("%d,%d=%Lf\n",i,j,C_ar[i][j]);
    }
  }
  return C_ar[m1-1][m2-1];
}

int mini_pos(long double a,long double b,long double c){
  if(a<b && a< c){
    return 1;
  }else if(b<a && b<c){
    return 2;
  }else{
    return 3;
  }
}

curvePtr AVG(int D,curvePtr p,curvePtr q, int numCurves, int c, int loop, int clusters){
  int i,j,max_points,final_points=0;
  max_points=p->points+q->points-1;
  int ppoints=p->points;
  int qpoints=q->points;
  int step=0;
  int min_pos;
  // printf("p=%d-q=%d\n",ppoints,qpoints);
  curvePtr curveAvg=malloc(sizeof(Curve));
  curveAvg->id=numCurves + c;
  curveAvg->points=max_points;
  curveAvg->is_centroid=1;
  curveAvg->has_centroid=-1;
  curveAvg->cenBuckets = NULL;
  curveAvg->coordinates=malloc(max_points*D*sizeof(long double));
  long double La[ppoints+1][qpoints+1];
  //printf("ok");
  //La=DFD_array(p,q,D);
  long double maxd, mind ,d;
  for(i=0;i<ppoints;i++){
    for(j=0;j<qpoints;j++){
      if(i==0 && j==0){
        La[i][j]=coor_dist(D,i,j,p->coordinates,q->coordinates);
        //L_ar[i][j]=dist(p->coordinates,q->coordinates,D);
        //printf("0,0=%Lf\n",La[i][j]);
      }else if (i==0){
        d=coor_dist(D,i,j,p->coordinates,q->coordinates);
        //d=dist(p->coordinates,q->coordinates,D);
        maxd=max(d,La[i][j-1]);
        La[i][j]=maxd;
        //printf("0,%d=%Lf\n",j,La[i][j]);
      }else if(j==0){
        d=coor_dist(D,i,j,p->coordinates,q->coordinates);
        //d=dist(p->coordinates,q->coordinates,D);
        maxd=max(d,La[i-1][j]);
        La[i][j]=maxd;
        //printf("%d,0=%Lf\n",i,La[i][j]);
      }else{
        d=coor_dist(D,i,j,p->coordinates,q->coordinates);
        //d=dist(p->coordinates,q->coordinates,D);
        mind=min(La[i-1][j],La[i][j-1],La[i-1][j-1]);
        maxd=max(d,mind);
        La[i][j]=maxd;
        //printf("%d,%d=%Lf\n",i,j,La[i][j]);
      }
    }
  }
  //printf("ok");
  ppoints--;
  qpoints--;
  // printf("p=%d-q%d\n",ppoints,qpoints);
    j=0;
    for(i=0;i<D;i++){
      curveAvg->coordinates[step]=(p->coordinates[ppoints*D+j]+q->coordinates[qpoints*D+j])/2;
      // printf("coor-avg[%d]=%Lf\n",step,curveAvg->coordinates[step]);
      j++;
      step++;
    }
  while(ppoints!=0 && qpoints!=0){
    final_points++;
    min_pos=mini_pos(La[ppoints-1][qpoints],La[ppoints][qpoints-1],La[ppoints-1][qpoints-1]);
    // printf("mp=%d\n",min_pos);
    if(min_pos==0){
      j=0;
      for(i=0;i<D;i++){
        curveAvg->coordinates[step]=(p->coordinates[ppoints*D+j]+q->coordinates[qpoints*D+j])/2;
        // printf("coor-avg[%d]=%Lf\n",step,curveAvg->coordinates[step]);
        j++;
        step++;
      }
      --ppoints;
      // printf("p=%d-q%d\n",ppoints,qpoints);
    }else if(min_pos==1){

      j=0;
      for(i=0;i<D;i++){
        curveAvg->coordinates[step]=(p->coordinates[ppoints*D+j]+q->coordinates[qpoints*D+j])/2;
        // printf("coor-avg[%d]=%Lf\n",step,curveAvg->coordinates[step]);
        j++;
        step++;
      }
      --qpoints;
      // printf("p=%d-q%d\n",ppoints,qpoints);
    }else{

      j=0;
      for(i=0;i<D;i++){
        curveAvg->coordinates[step]=(p->coordinates[ppoints*D+j]+q->coordinates[qpoints*D+j])/2;
        // printf("coor-avg[%d]=%Lf\n",step,curveAvg->coordinates[step]);
        j++;
        step++;
      }
      --ppoints;
      --qpoints;
      // printf("p=%d-q%d\n",ppoints,qpoints);
    }
  }
  if(ppoints==0){
    // printf("in\n");
    //qpoints--;
    while(qpoints>0){
      j=0;
      for(i=0;i<D;i++){
        curveAvg->coordinates[step]=(p->coordinates[ppoints*D+j]+q->coordinates[qpoints*D+j])/2;
        // printf("coor-avg[%d]=%Lf\n",step,curveAvg->coordinates[step]);
        j++;
        step++;
      }
      --qpoints;
      final_points++;
      // printf("p=%d-q%d\n",ppoints,qpoints);
    }
  }else if(qpoints==0){
    //ppoints--;
    while (ppoints>0){
      j=0;
      for(i=0;i<D;i++){
        curveAvg->coordinates[step]=(p->coordinates[ppoints*D+j]+q->coordinates[qpoints*D+j])/2;
        // printf("coor-avg[%d]=%Lf\n",step,curveAvg->coordinates[step]);
        j++;
        step++;
      }
      --ppoints;
      final_points++;
      // printf("p=%d-q%d\n",ppoints,qpoints);
    }
  }
  final_points++;
  curveAvg->points=final_points;
  // printf("finalp=%d\n",final_points);
  curveAvg->coordinates = (long double*)realloc(curveAvg->coordinates,step*sizeof(long double));
  return curveAvg;
}
