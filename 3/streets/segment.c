#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Curve* curvePtr;

typedef struct Curve{
  int no;
  long long int id;
  int points;
  long double* coordinates;
  char type[20];
}Curve;

int firstRead(){
  int numCurves = 0, points = 0;
  long long int id;
  char tag[2];
  memset(tag,'\0',sizeof(tag));
  int flag = 0;

  FILE *stream;
  FILE *outf = fopen("first", "w" );
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  stream = fopen("ways100", "r" );

  while ((read = getline(&line, &len, stream)) != -1) {
    if(flag == 1){
      flag = 0;
      continue;
    }
    line[read - 1] = 0;
    char *pch;

    pch = strtok(line, "=");
    strcpy(tag, pch);
    if(strcmp(tag, "id") == 0){
          points = 0;
          numCurves++;
          pch = strtok(NULL, "=");
          id = strtoll(pch, NULL, 10);
          fprintf(outf, "%lld\t", id);
    }
    else if(strcmp(tag, "n") == 0){
      points++;
      pch = strtok(NULL, "=");
    }
    else if(strcmp(tag, "v") == 0){
      fprintf(outf,"%d\n", points);
      flag = 1;
    }
  }

  printf("Curves are %d\n", numCurves);
  fclose(stream);
  fclose(outf);
  free(line);
  return numCurves;
}

void secondRead(curvePtr* curveAr){
  int i, j;
  int position = 0;

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  char type[20];

  stream = fopen("first", "r" );
  while ((read = getline(&line, &len, stream)) != -1) {
      line[read - 1] = 0;
      char *pch;

      curvePtr curveP = malloc(sizeof(Curve));
      if(curveP == NULL){
            printf("Error! memory not allocated.");
            return;
      }
      memset(curveP->type,'\0',sizeof(type));
      pch = strtok(line, "\t");
      curveP->id = strtoll(pch, NULL, 10);

      pch = strtok(NULL, "\t");
      curveP->points = atoi(pch);

      curveP->coordinates = (long double*)malloc( 2 * curveP->points * sizeof(long double));
      if(curveP->coordinates == NULL) {
            printf("Error! memory not allocated.");
            return;
      }

      //finished reading curve
      curveAr[position] = curveP;   //saved to array
      position++;
  }
  fclose(stream);
  free(line);
}

void thirdRead(curvePtr* curveAr){
  int i, j;
  int position = 0;
  char tag[2];
  memset(tag,'\0',sizeof(tag));

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  stream = fopen("athens100.csv", "r" );
  while ((read = getline(&line, &len, stream)) != -1) {
      line[read - 1] = 0;
      char *pch;

      pch = strtok(line, "\t"); //id
      // printf("id = %lld\n", curveAr[position]->id);
      pch = strtok(NULL, "\t"); //type
      strcpy(curveAr[position]->type, pch);
      // printf("%s\n", curveAr[position]->type);

      for(i = 0; i < 2*curveAr[position]->points; i++){
        pch = strtok(NULL, "\t"); //coordinate
        curveAr[position]->coordinates[i] = strtold(pch, NULL);
      }
      position++;
  }
  fclose(stream);
  free(line);
}

void segment(curvePtr* curveAr,int numCurves){
  int position = 0, cut = 0;
  int i,n,j;
  long double x_to_compare, y_to_compare;
  int crossroad = 0, cross_last = 0, cross_before = 0, count = -1;

  FILE *stream = fopen("initialsegment100.csv", "w" );

  while(position < numCurves){
    // printf("Checking curve %d:\n", position);
    count++;
    fprintf(stream, "%d,", count);
    fprintf(stream, "%lld", curveAr[position]->id);
    cut = 0;
    for(i = 0; i < 2*curveAr[position]->points; i++){//for each point
      cut++;
      // printf("Checking point %d\n",i );
      x_to_compare = curveAr[position]->coordinates[i];
      y_to_compare = curveAr[position]->coordinates[++i];
      if(cross_before == 0){
        if(cut > 10){
          count++;
          fprintf(stream, "\n");
          fprintf(stream, "%d,", count);
          fprintf(stream, "%lld", curveAr[position]->id);
          fprintf(stream, ",%Lf,%Lf", x_to_compare, y_to_compare);
          cut = 1;
        }
        else
          fprintf(stream, ",%Lf,%Lf", x_to_compare, y_to_compare);
      }

      for(j = 0; j < numCurves; j++){ //br
        if(j != position){ //for every curve except the one I am comparing
          // printf("Comparing with curve %d \n",j );
          for(n = 0; n < 2*curveAr[j]->points; n++){
            // printf("and point %d\n",n );
            if(x_to_compare == curveAr[j]->coordinates[n] && y_to_compare == curveAr[j]->coordinates[++n]){
              crossroad = 1; //crossroad needed for break
              if(cross_before == 0){
                cross_before = 1;
              }
              else if(cross_before == 1){
                fprintf(stream, ",%Lf,%Lf", x_to_compare, y_to_compare);
              }
              // printf("%Lf %Lf from curve %d = %Lf %Lf from curve %d\n",x_to_compare,y_to_compare,position,curveAr[j]->coordinates[n-1],curveAr[j]->coordinates[n],j );
              if(i == curveAr[position]->points - 1){
                cross_last = 1;
              }
              break;
            }
          }
          if(crossroad > 0){
            crossroad = 0;
            break;
          }
          //if no break happened until now
          if(j == numCurves - 1 && cross_before == 1){
            if(cut < 4){ // dont cut if it is too small
              fprintf(stream, ",%Lf,%Lf", x_to_compare, y_to_compare);
            }
            else{
              count++;
              fprintf(stream, "\n");
              fprintf(stream, "%d,", count);
              fprintf(stream, "%lld", curveAr[position]->id);
              fprintf(stream, ",%Lf,%Lf", x_to_compare, y_to_compare);
              cut = 1;
            }
            cross_before = 0;
          }
        }
    }
  }
  if(cross_last == 1){
    cross_last = 0;
    cross_before = 0;
    fprintf(stream, "\n");
  }
  else{
    fprintf(stream, "\n");
  }
  position++;
  }

  fclose(stream);
}

int aRead(){
  int numCurves = 0;

  FILE *stream;

  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  stream = fopen("initialsegment100.csv", "r" );

  while ((read = getline(&line, &len, stream)) != -1) {
    numCurves++;
  }

  printf("Curves are %d\n", numCurves);
  fclose(stream);
  free(line);
  return numCurves;
}

void bRead(curvePtr* curveAr){
  int i, j;
  int commas = 0;
  int no = 0;


  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  stream = fopen("initialsegment100.csv", "r" );
  while ((read = getline(&line, &len, stream)) != -1) {
      line[read - 1] = 0;
      char *pch;

      curvePtr curveP = malloc(sizeof(Curve));
      if(curveP == NULL){
            printf("Error! memory not allocated.");
            return;
      }

      pch = strtok(line, ",");
      curveP->no = no;
      pch = strtok(NULL, ",");
      curveP->id = strtoll(pch, NULL, 10);

      pch = strtok(NULL, ",");
      while(pch != NULL){
        commas++;
        pch = strtok(NULL, ",");
      }

      curveP->points = commas / 2;
      // printf("c %d\n", commas);
      // printf("p %d\n\n", curveP->points);
      commas = 0;

      curveP->coordinates = (long double*)malloc( 2 * curveP->points * sizeof(long double));
      if(curveP->coordinates == NULL) {
            printf("Error! memory not allocated.");
            return;
      }


      //finished reading curve
      curveAr[no] = curveP;   //saved to array
      no++;
  }
  fclose(stream);
  free(line);

}

void cRead(curvePtr* curveAr){
  FILE *stream;
  FILE *outf;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int no = 0;
  int i;

  stream = fopen("initialsegment100.csv", "r" );
  outf = fopen("segment100.csv", "w" );
  while ((read = getline(&line, &len, stream)) != -1) {
      line[read - 1] = 0;
      char *pch;

      pch = strtok(line, ","); //no
      fprintf(outf, "%s,", pch);
      pch = strtok(NULL, ","); //id
      fprintf(outf, "%s,", pch);

      // printf("points are %d\n", curveAr[no]->points);
      fprintf(outf, "%d", curveAr[no]->points);
      for(i = 0; i < 2*curveAr[no]->points; i++){
        pch = strtok(NULL, ","); //coordinate
        fprintf(outf, ",%s", pch);
      }
      fprintf(outf, "\n" );
      no++;
  }
  fclose(stream);
  fclose(outf);
  free(line);
}

int main(int argc, char *argv[]){
  int c, l, i, j, k;
  curvePtr* curveAr;
  curvePtr* newCurveAr;
  FILE *outf;

  int numCurves = firstRead(); //first read of input file

  //curveAr holds all curves from input
  curveAr = malloc(numCurves*sizeof(curvePtr));
  if(curveAr == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }


  secondRead(curveAr);

  thirdRead(curveAr);
  // for(i = 0; i < numCurves; i++){
  //   printf("id = %lld\n", curveAr[i]->id);
  //   for(j = 0; j < 2*curveAr[i]->points; j++){
  //     printf("%Lf\n", curveAr[i]->coordinates[j]);
  //   }
  // }

  segment(curveAr, numCurves);

  numCurves = aRead();
  newCurveAr = malloc(numCurves*sizeof(curvePtr));
  if(newCurveAr == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }

  bRead(newCurveAr);

  cRead(newCurveAr);

  return 0;
}
