#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cluster.h"
#include "prog.h"
#include "grid.h"
#include "hashTable.h"


long double minDistCen(curvePtr* centroidAr, Pinfo pinfo){
  int i;

  long double dist, min = 0.0;
  for(i = 0; i < pinfo->clusters; i++){
    if(i == pinfo->clusters-1)  break;
    curvePtr curve1 = centroidAr[i];
    curvePtr curve2 = centroidAr[i+1];
    if(pinfo->f == 1) dist = DFD_function(curve1, curve2, pinfo->D);
    else if(pinfo->f == 2) dist = DTW_function(curve1, curve2, pinfo->D);

    // printf("Distance between %d and %d: %Lf\n",curve1->id,curve2->id , dist);
    if(min == 0.0)  min = dist;
    else if(dist < min) min = dist;
  }
  return min;
}

int rangeSearchBucket(BucketPtr* HashTable, int l, double R, Pinfo pinfo, curvePtr* curveAr, curvePtr* centroidAr, int* currentCentroidAr){
  int i, j, cen_id, query_id, cur, cen, stop = 1;
  int cen_in_bucket = 0;
  curvePtr queryCurve, centroidCurve;
  long double dist, min = 0.0;
  BucketPtr current, cur_cen;

  for(i = 0; i < pinfo->numCurves/8; i++){ //for each bucket
    cen_in_bucket = currentCentroidAr[i];
    // printf("Bucket %d: %d centroids\n", i, cen_in_bucket);
    if(cen_in_bucket > 0){
      current = HashTable[i]; //start
      //jump centroids
      for(j = cen_in_bucket; j > 0; j--){
        current = current->next;
      }
      while(current != NULL){ //check all bucket
        //query curve id
        query_id = current->id;
        //query curve pointer
        for(cur = 0; cur < pinfo->numCurves; cur++){
          if(curveAr[cur]->id == query_id){
            //check if it has already a center
            if(curveAr[cur]->has_centroid == -1){
              queryCurve = curveAr[cur];
            }
            else{
              // printf("Curve %d already has a center (%d)\n",query_id, curveAr[cur]->has_centroid);
              current->has_centroid = curveAr[cur]->has_centroid;
            }
            break;
          }
        }
        //check for flag first
        if(current->has_centroid == -1){
          stop = 0;

          //find dist for each centroid
          cur_cen = HashTable[i];
          for(j = 0; j < cen_in_bucket; j++){
            //centroid curve id
            cen_id = cur_cen->id;
            //centroid curve pointer
            for(cen = 0; cen < pinfo->clusters; cen++){
              if(centroidAr[cen]->id == cen_id){
                centroidCurve = centroidAr[cen];
                break;
              }
            }

            //Distance
            if(pinfo->f == 1) dist = DFD_function(centroidCurve, queryCurve, pinfo->D);
            else if(pinfo->f == 2) dist = DTW_function(centroidCurve, queryCurve, pinfo->D);

            // printf("Distance between \033[22;31m%d\033[m and %d: %Lf\n",centroidCurve->id, queryCurve->id , dist);

            if(dist <= R){
              if(min == 0.0){
                min = dist;
                current->has_centroid = cen_id;
                queryCurve->has_centroid = cen_id;
              }
              else if(dist < min){
                min = dist;
                current->has_centroid = cen_id;
                queryCurve->has_centroid = cen_id;
              }
            }

            //next centroid
            min = 0.0;
            cur_cen = cur_cen->next;
          }
        }
        //next curve
        current = current->next;
      }
    }
    cen_in_bucket = 0;
    //next bucket
  }
  return stop;
}

void checkUnassigned(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo){
  int i, query_id, cur, cen_id, j;
  BucketPtr current;
  curvePtr queryCurve;
  long double dist, min = 0.0;

  for(cur = 0; cur < pinfo->numCurves; cur++){
    if(curveAr[cur]->is_centroid == -1 && curveAr[cur]->has_centroid == -1){
      // printf("curve %d is unassigned\n", curveAr[cur]->id);
      queryCurve = curveAr[cur];

      //for each centroid
      for(j = 0; j < pinfo->clusters; j++){
        //Distance
        if(pinfo->f == 1) dist = DFD_function(centroidAr[j], queryCurve, pinfo->D);
        else if(pinfo->f == 2) dist = DTW_function(centroidAr[j], queryCurve, pinfo->D);

        if(min == 0.0){
          min = dist;
          queryCurve->has_centroid = centroidAr[j]->id;
        }
        else if(dist < min){
          min = dist;
          queryCurve->has_centroid = centroidAr[j]->id;
        }
      }
      min = 0.0;
    }
  }
}

//Lloyd's
void lloyds(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo){
  fflush(stdout);
  int D = pinfo->D;
  int k = pinfo->clusters;
  int numCurves = pinfo->numCurves;
  long double dist, min = 0.0;
  int i, j;


  for(i = 0; i < numCurves; i++){//for each curve
    if(curveAr[i]->is_centroid == -1){//non centroid
      for(j = 0; j < k; j++){//for each centroid
        //Distance
        if(pinfo->f == 1) dist = DFD_function(centroidAr[j], curveAr[i], pinfo->D);
        else if(pinfo->f == 2) dist = DTW_function(centroidAr[j], curveAr[i], pinfo->D);

        //keep min
        if(min == 0.0){
          min = dist;
          curveAr[i]->has_centroid = centroidAr[j]->id;
        }
        else if(dist < min){
          min = dist;
          curveAr[i]->has_centroid = centroidAr[j]->id;
        }
      }
    }
    //next curve
    min = 0.0;
  }
}
