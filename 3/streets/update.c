#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "cluster.h"
#include "grid.h"
#include "prog.h"
#include "hashTable.h"

int globalVar = 0;

//Objective Function
long double ObjectiveFunctionA(curvePtr* curveAr, curvePtr* candidates, Pinfo pinfo){
  long double sum = 0.0;
  int i, c;
  for(c = 0; c < pinfo->clusters; c++){
    for(i = 0; i < pinfo->numCurves; i++){
      if(curveAr[i]->candidate == candidates[c]->id){
        if(pinfo->f == 1) sum = sum + DFD_function(curveAr[i], candidates[c], pinfo->D);
        else if(pinfo->f == 2) sum = sum + DTW_function(curveAr[i], candidates[c], pinfo->D);
      }
    }
  }

  return sum;
}

long double ObjectiveFunctionB(curvePtr centroid, curvePtr candidate, BaseList baselist, Pinfo pinfo){
  long double sum = 0.0;
  int i, c;
  BaseList current = baselist;
  while(current != NULL){
    if(current->clusterCurve->id != candidate->id){
      if(pinfo->f == 1) sum = sum + DFD_function(candidate, current->clusterCurve, pinfo->D);
      else if(pinfo->f == 2) sum = sum + DTW_function(candidate, current->clusterCurve, pinfo->D);
    }
    current = current->next;
  }
  if(centroid->id != candidate->id){
    if(pinfo->f == 1) sum = sum + DFD_function(candidate, centroid, pinfo->D);
    else if(pinfo->f == 2) sum = sum + DTW_function(candidate, centroid, pinfo->D);
  }
  return sum;
}

BaseList createBaseList(curvePtr* curveAr, curvePtr centroid, Pinfo pinfo, int* sumClusterCurves){
  int i, f = 1;
  int numCurves = pinfo->numCurves;

  BaseList head = NULL;
  BaseList new = NULL;

  for(i = 0; i < numCurves; i++){
    if(curveAr[i]->has_centroid == centroid->id){
      *sumClusterCurves = *sumClusterCurves + 1;

      new = malloc(sizeof(BaseListNode));

      new->clusterCurve = curveAr[i];
      new->next = head;

      head = new;
    }
  }
  return head;
}

void freeBaseList(BaseList baseList){
  BaseList temp;
  BaseList current = baseList;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}

//a. Mean Discrete Frechet Curve
void updateMDFC(int loop, curvePtr* curveAr, curvePtr centroid, Pinfo pinfo, int c, curvePtr updatedCenter){
  curvePtr new;
  int j;
  //for this cluster create a baseList with clusterCurves
  int sumClusterCurves = 0;
  BaseList baseList = createBaseList(curveAr, centroid, pinfo, &sumClusterCurves);
  // printf("sum clusterCurves %d\n",sumClusterCurves );

  if(sumClusterCurves != 0){
    //create empty binary Tree AND insert baseList
    int height = findHeight(sumClusterCurves);
    // printf("height = %d\n", height);
    nodeTreePtr root = createBinaryTree(height, baseList);
    globalVar = 0;

    if(root != NULL){
      new = PostOrderTraversal(root, pinfo, c, loop);
      updatedCenter->id = new->id;
      updatedCenter->points = new->points;
      updatedCenter->is_centroid = new->is_centroid;
      updatedCenter->has_centroid = new->has_centroid;
      updatedCenter->cenBuckets = new->cenBuckets;
      updatedCenter->coordinates = malloc(new->points*pinfo->D*sizeof(long double));
      for(j = 0; j < new->points*pinfo->D; j++){
        updatedCenter->coordinates[j] = new->coordinates[j];
      }

    }

    // print2D(root);
    freeBaseList(baseList);
    if(root != NULL)    freeBinaryTree(root, pinfo->numCurves);
  }
  else{
    updatedCenter->id = -1;
    updatedCenter->coordinates = malloc(1 *pinfo->D* sizeof(long double));
  }
}

curvePtr MeanDiscreteFrechetCurve(curvePtr curveA, curvePtr curveB, Pinfo pinfo, int c, nodeTreePtr node, int loop){
  curvePtr mean;

  int D = pinfo->D;
  int numCurves = pinfo->numCurves;


  if(curveA != NULL && curveB != NULL){
    // printf("MDFC between %d and %d\n",curveA->id, curveB->id );
    mean = AVG(D, curveA, curveB, numCurves, c, loop, numCurves/8);
    node->flag = 1;
    return mean;
  }
  if(curveA == NULL && curveB != NULL){
    // printf("*** %d\n", curveB->id);
    return curveB;
  }

  if(curveA != NULL && curveB == NULL){
    // printf("*** %d\n", curveA->id);
    return curveA;
  }
  if(curveA == NULL && curveB == NULL)   return NULL;

}

curvePtr PostOrderTraversal(nodeTreePtr node, Pinfo pinfo, int c, int loop){
  curvePtr leftCurve, rightCurve, new;

  int D = pinfo->D;
  int numCurves = pinfo->numCurves;

  if(node->curve != NULL){
    // printf("found %d\n", node->curve->id);
    return node->curve;
  }
  else{

    if(node->right != NULL) rightCurve = PostOrderTraversal(node->right, pinfo, c, loop);
    else return NULL;

    if(node->left != NULL) leftCurve = PostOrderTraversal(node->left, pinfo, c, loop);
    else  return NULL;

    new = MeanDiscreteFrechetCurve(leftCurve, rightCurve, pinfo, c, node, loop);
    node->curve = new;
    return new;
  }
}

void print2DUtil(nodeTreePtr root, int space){
    // Base case
    if (root == NULL)
        return;

    // Increase distance between levels
    space += COUNT;

    // Process right child first
    print2DUtil(root->right, space);

    // Print current node after space
    // count
    printf("\n");
    for (int i = COUNT; i < space; i++)
        printf(" ");
    if(root->curve == NULL) printf("*\n");
    else  printf("%d[%d]\n", root->curve->id, root->flag);


    // Process left child
    print2DUtil(root->left, space);
}

void print2D(nodeTreePtr root){
   // Pass initial space count as 0
   print2DUtil(root, 0);
}

int findHeight(int a){
  double val = a;
  int height = 1;
  while(a > 1){
    val = ceil(val/2);
    a = val;
    height++;
  }
  return height;
}

nodeTreePtr createBinaryTree(int height, BaseList baseList){
  int i;
  nodeTreePtr current;
  BaseList wanted = baseList;
  // printf("height is %d\n",height );
  if (height <= 0)
      return NULL;

  current = malloc(sizeof(nodeTree));

  if(height == 1){
    if(globalVar != 0){
      for(i = 0; i < globalVar; i++){//find node needed from baseList
        if(wanted == NULL)  wanted = NULL;
        else  wanted = wanted->next;
      }
    }
    globalVar++;
    if(wanted != NULL){
      // printf("insert %d\n", wanted->clusterCurve->id);
      current->curve = wanted->clusterCurve;
      current->flag = 1;
    }
    else{
      current->curve = NULL;
      current->flag = 0;
    }
  }
  else{
    current->curve = NULL;
    current->flag = 0;
  }
  current->right = createBinaryTree(height-1, baseList);
  current->left = createBinaryTree(height-1, baseList);
  return current;
}

void freeBinaryTree(nodeTreePtr node, int numCurves){
    if(node == NULL)
      return;


    freeBinaryTree(node->right, numCurves);
    freeBinaryTree(node->left, numCurves);

    if(node->curve != NULL && node->flag == 1){
      // printf("freed NODE %d\n", node->curve->id);
      if(node->curve->id >= numCurves){
        free(node->curve->coordinates);
        free(node->curve);
        node->curve = NULL;
      }
    }
    // else    printf("freed NULL node\n");
    free(node);
}

//b. PAM
curvePtr updatePAM(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo){
  int c, swap, i , j, d, changed, sumClusterCurves;
  BaseList baseList;
  long double min = 0.0, new_objective;
  curvePtr new, min_centroid;
  int clusters = pinfo->clusters;
  int numCurves = pinfo->numCurves;

  for(c = 0; c < clusters; c++){
    sumClusterCurves = 0;

    baseList = createBaseList(curveAr, centroidAr[c], pinfo, &sumClusterCurves);
    // printf("sum is %d\n",sumClusterCurves );

    if(baseList != NULL){
      //return best of cluster
      // printf("cluster %d\n",c );
      new_objective = 0.0;
      new = ClusterPAM(curveAr, centroidAr[c], pinfo, baseList, &new_objective);
      // printf("in cluster %d min is %d with value %Lf\n",c, new->id, new_objective );
      //find min of all clusters
      if(min == 0.0){
        min = new_objective;
        min_centroid = new;
      }
      else if(new_objective < min){
        min = new_objective;
        min_centroid = new;
      }
    }
    freeBaseList(baseList);
  }
  // printf("new is %d\n", min_centroid->id);
  // printf("from cluster with centroid %d\n", min_centroid->has_centroid);
  // printf("new objective %Lf\n",min );
  return min_centroid;
}

curvePtr ClusterPAM(curvePtr* curveAr, curvePtr centroid, Pinfo pinfo, BaseList baseList, long double* new_objective){
  curvePtr new;
  long double objective, min = 0.0;

  BaseList current = baseList;

  //Objective Function with current center
  min = ObjectiveFunctionB(centroid, centroid, baseList, pinfo);
  // printf("initial %Lf\n", min);
  new = centroid;

  while(current != NULL){
    objective = 0.0;
    //checking current point of cluster
    objective = ObjectiveFunctionB(centroid, current->clusterCurve, baseList, pinfo);
    // printf("new %Lf\n", objective);
    //find min of cluster
    if(objective < min){
      min = objective;
      new = current->clusterCurve;
    }
    current = current->next;
  }
  *new_objective = *new_objective + min;
  return new;
}


//2.4
void LSHclustering(int* ar_ri, BucketPtr** LSH_Ar, curvePtr* curveAr, Pinfo pinfo){

  int i, l, c, key, a;
  int rep = 0, stop = 0, cen_in_bucket = 0;
  nodePtr g;

  int K = pinfo->K;
  int L = pinfo->L;
  int D = pinfo->D;
  int clusters = pinfo->clusters;
  int numCurves = pinfo->numCurves;
  int maxPoints = pinfo->maxPoints;

  //index all
  for(i = 0; i < numCurves; i++){
    g = grid(curveAr[i], pinfo);
    for(l = 0; l < L; l++){
      LSH_Hash(g, numCurves, LSH_Ar[l], curveAr[i], ar_ri, D, l);
    }
  }

  // for(l = 0; l < L; l++){
  //   printHashTable(LSH_Ar[l], numCurves);
  // }
}

void findBucketMean(BucketPtr* HashTable, curvePtr* curveAr, Pinfo pinfo, curvePtr* meanCenters){
  //for each bucket create a baseList with clusterCurves
  curvePtr new;
  int sumClusterCurves = 0, loop = 0;
  int i, j, l;
  int numCurves = pinfo->numCurves;

  for(i = 0; i < numCurves / 8; i++){
    meanCenters[i] = malloc(sizeof(Curve));
  }
  //for each bucket create a BaseList
  for(i = 0; i < numCurves / 8; i++){
    BaseList head = NULL;
    BaseList newB = NULL;

    BucketPtr current = HashTable[i];

    if(current->id != -1){
      while(current != NULL){
        sumClusterCurves++;

        newB = malloc(sizeof(BaseListNode));

        for(j= 0; j < numCurves; j++){
          if(curveAr[j]->id == current->id){
            newB->clusterCurve = curveAr[j];
            break;
          }
        }
        newB->next = head;
        head = newB;

        current = current->next;
      }
    }
    else head = NULL;

    //baseList ready
    BaseList cur = head;
    // printf("BUCKET %d: ",i );
    // while(cur != NULL){
    //   printf("%d -> ", cur->clusterCurve->id);
    //   cur = cur->next;
    // }printf("\n" );

    // printf("sum clusterCurves %d\n",sumClusterCurves );

    if(sumClusterCurves != 0){
      //create empty binary Tree AND insert baseList
      int height = findHeight(sumClusterCurves);
      // printf("height = %d\n", height);
      nodeTreePtr root = createBinaryTree(height, head);
      globalVar = 0;

      if(root != NULL){
        new = PostOrderTraversal(root, pinfo, i, loop);
        meanCenters[i]->id = new->id;
        meanCenters[i]->points = new->points;
        meanCenters[i]->is_centroid = new->is_centroid;
        meanCenters[i]->has_centroid = new->has_centroid;
        meanCenters[i]->cenBuckets = new->cenBuckets;
        meanCenters[i]->coordinates = malloc(new->points*pinfo->D*sizeof(long double));
        for(j = 0; j < new->points*pinfo->D; j++){
          meanCenters[i]->coordinates[j] = new->coordinates[j];
        }
        loop++;
      }
      // print2D(root);
      freeBaseList(head);
      if(root != NULL)    freeBinaryTree(root, pinfo->numCurves);

      sumClusterCurves = 0;
    }
    else{
      meanCenters[i]->id = -1;
      meanCenters[i]->coordinates = malloc(1 *pinfo->D* sizeof(long double));
    }
  }
}
