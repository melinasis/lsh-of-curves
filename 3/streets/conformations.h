#include <stdio.h>
#include <stdlib.h>

void minusCenters(Pinfo pinfo, curvePtr* curveAr,curvePtr* centroidAr);
double cRMSDfunction(Pinfo pinfo, double *A, double *B, int crmsd_min);
double crmsd( double *x, double *y);
double dfdp3(double* p,double* q,Pinfo pinfo, int crmsd_min);
long double coor_dist(int D,int i,int j,long double *p,long double *q);
