#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "prog.h"
#include "grid.h"
#include "hashTable.h"

BucketPtr* createHashTable(int numCurves){

  BucketPtr* HashTable = malloc(((numCurves)/8)*sizeof(BucketPtr));
  if(HashTable == NULL)
  {
        printf("Error! memory not allocated.");
        EXIT_FAILURE;
  }
  int i;
  for(i = 0; i < (numCurves)/8; i++){
    HashTable[i] = malloc(sizeof(BucketList));
    if(HashTable[i] == NULL)
    {
          printf("Error! memory not allocated.");
          EXIT_FAILURE;
    }
    HashTable[i]->id = -1;
    HashTable[i]->g = NULL;
    HashTable[i]->next = NULL;
    HashTable[i]->is_centroid = -1;
    HashTable[i]->has_centroid = -1;

  }
  return HashTable;
}

void insertHashTable(BucketPtr* HashTable, int key, curvePtr curveP, nodePtr g){
  BucketPtr current = HashTable[key];
  if(current->id == -1){    //first node
    current->id = curveP->id;
    current->g = g;
    current->is_centroid = curveP->is_centroid;
  }
  else{
    while(current->next != NULL){
      current = current->next;
    }
    current->next = malloc(sizeof(BucketList));
    if(current->next == NULL)
    {
          printf("Error! memory not allocated.");
          EXIT_FAILURE;
    }
    current->next->id = curveP->id;
    current->next->g = g;
    current->next->is_centroid = curveP->is_centroid;
    current->next->has_centroid = -1;
    current->next->next = NULL;
  }

}

void printHashTable(BucketPtr* HashTable, int numCurves){
  int i;
  for(i = 0; i < numCurves / 8; i++){   //HashTable cells
    printf("BUCKET %d:    ", i);
    BucketPtr current = HashTable[i];
    if(current->id != -1){
      if(current->is_centroid == 1){
        printf("\033[22;31m%d\033[m ",current->id );
      }
      else if(current->has_centroid != -1){
        printf("\033[22;34m%d\033[m ",current->id);
      }
      else  printf("%d  ", current->id);
    }
    while(current->next != NULL){
      current = current->next;
      if(current->id != -1){
        if(current->is_centroid == 1){
          printf("\033[22;31m%d\033[m ",current->id );
        }
        else if(current->has_centroid != -1){
          printf("\033[22;34m%d\033[m ",current->id);
        }
        else  printf("%d  ", current->id);
      }
    }
    printf("\n");
  }
}

void resetHashTable(BucketPtr* HashTable, int numCurves){
  int i;
  for(i = 0; i < numCurves / 8; i++){   //HashTable cells
    BucketPtr current = HashTable[i];
    while(current != NULL){
      current->is_centroid = -1;
      current->has_centroid = -1;
      current = current->next;
    }
  }
}

void freeBucketList(BucketPtr head, int l){
  BucketPtr temp;
  BucketPtr current = head;
  while(current != NULL){
    if(l == 0){
      freeGridList(current->g);
    }
    temp = current;
    current = current->next;
    free(temp);
  }
}

void freeHashTable(BucketPtr* HashTable, int numCurves, int l){
  int i;
  for(i = 0; i < numCurves/8; i++){
    freeBucketList(HashTable[i], l);
  }
  free(HashTable);
}

int maxBuck(BucketPtr head){
  int maxBucket = 0;
  BucketPtr current = head;
  if(current->id != -1){
    while(current != NULL){
      maxBucket++;
      current = current->next;
    }
  }
  return maxBucket;
}
