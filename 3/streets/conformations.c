#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <lapacke.h>
#include <cblas.h>
#include "prog.h"
#include "grid.h"
#include "hashTable.h"
#include "cluster.h"
#include "conformations.h"

double p3coor_dist(int D,int i,int j,double *p,double *q){
  fflush(stdout);
  long double diff,power,sqr,sum=0;
  int k;
    for(k=0;k<D;k++){
      //printf("p:%Lf-q:%Lf\n",p[i+k],q[j+k]);
      diff=(p[i+k])-(q[j+k]);
      //printf("diff=%Lf\n",diff);
      power=pow(diff,2);
      //printf("pow=%Lf\n",power);
      sum =sum+power;
    }
    sqr=sqrt(sum);
    //printf("sqr=%Lf\n",sqr);
  return sqr;
}

double dfdp3(double *p,double *q,Pinfo pinfo,int crmsd_min){
  double maxd, mind ,d;
  //int m = q->points;
  //int n = p->points;
  int n,m;
  n=crmsd_min*pinfo->D;
  m=n;
  int D=pinfo->D;
   double L_ar[n][m];
  int i,j;

  for(i = 0; i < n; i++){
    for(j = 0; j < m; j++){
      if(i == 0 && j == 0){
        L_ar[i][j]=p3coor_dist(D,i,j,p,q);
        //L_ar[i][j]=dist(p->coordinates,q->coordinates,D);
        // printf("0,0=%Lf\n",L_ar[i][j]);
      }else if (i==0){
        d=p3coor_dist(D,i,j,p,q);
        //d=dist(p->coordinates,q->coordinates,D);
        maxd=max(d,L_ar[i][j-1]);
        L_ar[i][j]=maxd;
        // printf("0,%d=%Lf\n",j,L_ar[i][j]);
      }else if(j==0){
        d=p3coor_dist(D,i,j,p,q);
        //d=dist(p->coordinates,q->coordinates,D);
        maxd=max(d,L_ar[i-1][j]);
        L_ar[i][j]=maxd;
        // printf("%d,0=%Lf\n",i,L_ar[i][j]);
      }else{
        d=p3coor_dist(D,i,j,p,q);
        //d=dist(p->coordinates,q->coordinates,D);
        mind=min(L_ar[i-1][j],L_ar[i][j-1],L_ar[i-1][j-1]);
        maxd=max(d,mind);
        L_ar[i][j]=maxd;
        // printf("%d,%d=%Lf\n",i,j,L_ar[i][j]);
      }
    }
  }
  // printf("final table=%Lf\n",L_ar[n-1][m-1]);
  return L_ar[n-1][m-1];
}

double crmsd( double *x, double *y){
   double sum=0.0,power,f,rmsd;
  int i;
  for(i=0;i<3;i++){
    power=pow(x[i]-y[i],2);
    // printf("power=%f\n",power);
    sum+=sum+power;
  }
  // printf("sum=%f\n",sum);
  f=sum/3;
  rmsd=sqrt(f);
  // printf("RMSD=%f\n",rmsd);
  return rmsd;
}

double cRMSDfunction(Pinfo pinfo, double *A, double *B, int crmsd_min){
  //fflush(stdout)
  int N = crmsd_min * pinfo->D;
  lapack_int n = N;
  lapack_int ipiv[N],ipiv2[N];
  //A=malloc(N*sizeof(double));
  //B=malloc(N*sizeof(double));
  //double *reverse_x;
  int i,j;
  double ALPHA=1.0,BETA=0.0;
  for(i=0;i<N;i++){
    // printf("x %d=%f\n",i,A[i]);
  }
  lapack_int NROWS = n;
  lapack_int NCOLS = 1;
  lapack_int LEADING_DIMENSION_A = n;
  lapack_int reverse_x;
  //lapack_int * rx;
  //reverse_x = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, NROWS, NCOLS, A, LEADING_DIMENSION_A, ipiv);
  //own reverse func
  if(reverse_x==0){
    // printf("dgertf ok\n");
  }
  for(i=0;i<N;i++){
    // printf("x %d=%f\n",i,A[i]);
    //printf("ipiv:%d\n",ipiv[i]);
  }
  double *C;
  int nRows=1,nCols=N,lda=1,ldb=N,ldc=N,k=1;
  C = (double*) malloc(nRows * nCols * sizeof(double));
  //own multiply func
  cblas_dgemm (CblasRowMajor, CblasNoTrans, CblasNoTrans, nRows,  nCols, k, ALPHA, A , lda, B, ldb, BETA, C, ldc);
  for(i=0;i<nRows*nCols;i++){
    // printf("c %d=%f\n",i,C[i]);
  }
  double *singular,*v,*stat;
  lapack_int info,idin,ldv;
  idin=N;
  ldv=N;
  singular=LAPACKE_malloc(sizeof(double)*n);
  stat=LAPACKE_malloc(sizeof(double)*n*2);
  //own  svd func
  info=LAPACKE_dgesvj(LAPACK_ROW_MAJOR,'G','N','N', NROWS, NCOLS,C,idin,singular,0,v,ldv,stat);
  for(i=0;i<n;i++){
    // printf("singular:%f\n",singular[i]);
    //printf("stat?:%f\n",stat[i]);
    //printf("v:%f\n",v[i]);
  }
  if(singular[2]>0){
    // printf("something happens\n");
  }

  double *Q;
  Q=(double*)malloc(nRows * nCols * sizeof(double));
  int ldvr=1,lds=N,ldq=N;
  //double *reverse_v;
  lapack_int reverse_v;
  //printf("seg message\n");
  //f
  //reverse_v = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, NROWS, NCOLS, C, LEADING_DIMENSION_A, ipiv2);
  //printf("dgetrf not seg\n");
  for(i=0;i<nRows*nCols;i++){
    //for(j=0;j<n;j++){
      //printf("seg here?\n");
      // printf("C %d=%f\n",i,C[i]);
    //}
  }
  //f

  cblas_dgemm (CblasRowMajor, CblasNoTrans, CblasNoTrans, nRows,  nCols, k, ALPHA, C, ldvr, A, lds, BETA, Q, ldq);
  for(i=0;i<nRows*nCols;i++){
    //for(j=0;j<n;j++){
      // printf("q %d=%f\n",i,Q[i]);
    //}
  }
  if(Q[n-1]<0){
    // printf("q%d:%f\n",n-1,Q[n-1]);
    Q[n-1]=-Q[n-1];
    // printf("q%d:%f\n",n-1,Q[n-1]);
  }
  double rmsd;
  rmsd=dfdp3(Q,B,pinfo,crmsd_min);


  //
  free(C);
  free(singular);
  free(stat);
  free(Q);
 return rmsd;
}

void minusCenters(Pinfo pinfo, curvePtr* curveAr, curvePtr* centroidAr){
  int numCurves = pinfo->numCurves;
  int i, j,c, points, center_id;
  curvePtr center;
  int D = 2;

  for(i = 0; i < numCurves; i++){
    points = curveAr[i]->points;
    if(curveAr[i]->is_centroid == -1){
      center_id = curveAr[i]->has_centroid;
      // printf("center is %d\n", center_id);
      for(c = 0; c < pinfo->clusters; c++){
        if(centroidAr[c]->id == center_id){
          center = centroidAr[c];
        }
      }
    }
    for(j = 0; j < D*points; j++){
      if(curveAr[i]->is_centroid == -1){
        curveAr[i]->minus_centers[j] = curveAr[i]->coordinates[j] - center->coordinates[j];
        // printf("%Lf - %Lf= %f\n", curveAr[i]->coordinates[j] , center->coordinates[j], curveAr[i]->minus_centers[j]);
      }
    }
  }
}
