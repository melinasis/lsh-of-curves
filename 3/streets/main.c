#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "prog.h"
#include "grid.h"
#include "conformations.h"
#include "hashTable.h"
#include "cluster.h"

int main(int argc, char *argv[]){

  int c, l, i, j, k, clusters;
  nodePtr g = NULL;
  FILE *outf;
  FILE *lsh_out = fopen("lsh_ways_clustering.dat", "w" );

  int** centroidL;
  int* ar_ri;
  curvePtr* candidates;
  curvePtr* centroidAr;

  long double obj_fun = -1.0, new_obj_fun;
  int stop_update = 0, loop = 0;
  int a = -1, ok = -1;

  Pinfo pinfo = malloc(sizeof(Prog));
  if(pinfo == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }

  if (argHandling(pinfo, argc, argv) == EXIT_FAILURE){
  		printHelp(argv[0]);
  		return EXIT_FAILURE;
  }
  confHandling(pinfo);  //read from configuration
  outf = fopen(pinfo->output,"w");  //output file
  // fprintf(outf,"HashFunction: Propabilistic\n");
  // if(pinfo->f == 1) fprintf(outf,"Distance Function: DFD\n" );
  // else if(pinfo->f == 2)  fprintf(outf,"Distance Function: CRMSD\n" );


  firstRead(pinfo, outf); //first read of input file
  //finds D,delta, numCurves, N

  int K = pinfo->K;
  int L = pinfo->L;
  int D = pinfo->D;
  clusters = pinfo->clusters;
  int numCurves = pinfo->numCurves;
  int maxPoints = pinfo->maxPoints;


  //curveAr holds all curves from input
  curvePtr* curveAr = malloc(numCurves*sizeof(curvePtr));
  if(curveAr == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }
  //centroidAr holds all centroid curves
  centroidAr = malloc(clusters*sizeof(curvePtr));
  if(centroidAr == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }
  //LSH_Ar holds all HashTables for non centroid curves
  BucketPtr** LSH_Ar = malloc(L * sizeof(BucketPtr*));
  if(LSH_Ar == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }
  for(l = 0; l < L; l++){LSH_Ar[l] = createHashTable(numCurves);}

  secondRead(pinfo, curveAr);//second read of input file
  //stores input to curveAr

while(ok == -1){
                     //INITIALIZATION
 stop_update = 0;
 clusters = pinfo->clusters;
 centroidAr = realloc(centroidAr, clusters*sizeof(curvePtr));

  // fprintf(outf,"\nINITIALIZATION: K-means\n");
  kmeans(curveAr, centroidAr, pinfo);


  // for(j = 0; j < clusters; j++){
  //   printf("%d\n", centroidAr[j]->id);
  // }


                        //ASSIGNMENT
  while(stop_update == 0){
    // fprintf(outf,"ASSIGNMENT: Lloyd's\n");
    lloyds(curveAr, centroidAr, pinfo);

    // for(c = 0; c < clusters; c++){
    //   printf("\033[22;31m%d\033[m: ",centroidAr[c]->id );
    //   for(i = 0; i < numCurves; i++){
    //     if(curveAr[i]->has_centroid == centroidAr[c]->id){
    //       printf("%d ", curveAr[i]->id);
    //     }
    //   }
    //   printf("\n" );
    // }printf("\n");

                      //CRMSD FUNCTIONS
    if(a == 1){
      int crmsd_min;
      minusCenters(pinfo, curveAr, centroidAr);

      double total_crmsd_dist = 0.0, gtpTemp;

      for(i = 0; i < numCurves -1; i++){
        if(curveAr[i]->points <= curveAr[i+1]->points)  crmsd_min = curveAr[i]->points;
        else  crmsd_min = curveAr[i+1]->points;


        gtpTemp = cRMSDfunction(pinfo, curveAr[i]->minus_centers, curveAr[i+1]->minus_centers, crmsd_min);
        total_crmsd_dist += gtpTemp;
        // printf("temp total crmsd:%f\n",total_crmsd_dist);
      }

      a=-1;
    }


                      //END OF ASSIGNMENT

    // for(i = 0; i < numCurves; i++){
    //   if(curveAr[i]->has_centroid != -1){
    //     curveAr[i]->candidate = curveAr[i]->has_centroid;
    //   }
    // }
                                  //UPDATE

    // fprintf(outf,"UPDATE: PAM\n\n");
    curvePtr new;
    new = updatePAM(curveAr, centroidAr, pinfo);
    // printf("new is %d\n",new->id );
    if(new != NULL){
      if(new->has_centroid != -1){
        for(c = 0; c < clusters; c++){
          if(centroidAr[c]->id == new->has_centroid){
            centroidAr[c]->is_centroid = -1;
            new->has_centroid = -1;
            new->is_centroid = 1;

            centroidAr[c] = new;
          }
        }
        //initialize all Curves to have no centroids
        for(i = 0; i < numCurves; i++){
          if(curveAr[i]->is_centroid == -1){
            curveAr[i]->has_centroid = -1;
          }
        }
        loop = loop + 1;
      }
      else{
        stop_update = 1;
      }
    }

    // for(c = 0; c < clusters; c++){
    //   printf("\033[22;31m%d\033[m: ",centroidAr[c]->id );
    //   for(i = 0; i < numCurves; i++){
    //     if(curveAr[i]->has_centroid == centroidAr[c]->id){
    //       printf("%d ", curveAr[i]->id);
    //     }
    //   }
    //   printf("\n" );
    // }printf("\n");
  }

                            //SILHOUETTE
  ok = silhouette(pinfo, curveAr, centroidAr, outf, 1);

  // printf("with clusters %d silhouette returned %d\n", clusters, ok);
  if(ok == -1){
    pinfo->clusters++;
    //initialize all
    for(i = 0; i < numCurves; i++){
      curveAr[i]->is_centroid = -1;
      curveAr[i]->has_centroid = -1;
    }
  }

}

      //OUTPUT FILE
int sum = 0;
for(c = 0; c < clusters; c++){
  // fprintf(outf, "CLUSTER %d {", c);
  if(centroidAr[c]->id >= numCurves){
    // fprintf(outf, "New center coordinates: " );
    for(j = 0; j < centroidAr[c]->points; j++){
      // fprintf(outf, "%Lf ", centroidAr[c]->coordinates[j]);
    }
    // fprintf(outf, "\n");
  }
  // fprintf(outf, "centroid: %d (",centroidAr[c]->id );
  for(i = 0; i < numCurves; i++){
    if(curveAr[i]->has_centroid == centroidAr[c]->id){
      fprintf(outf, "%d\t", curveAr[i]->id);
      sum = sum + 1;
    }
  }
  fprintf(outf, "\n");
  // fprintf(outf, ")}\tsize: %d\n\n", sum);
  sum = 0;
}

//initialize all
for(i = 0; i < numCurves; i++){
  curveAr[i]->is_centroid = -1;
  curveAr[i]->has_centroid = -1;
}

//CLUSTERING USING LSH
if(pinfo->f == 2){
  printf("Kmeans_ways ready\n" );
  printf("To run lsh_ways_clustering choose DFD\n" );
}
else if(pinfo->f == 1){
  printf("Kmeans_ways ready\n" );
  printf("Continuing with lsh_ways_clustering...\n" );

  ok = -1;

  //ar_ri
  ar_ri = malloc(K*maxPoints*D * sizeof(int));
  if(ar_ri == NULL){
        printf("Error! memory not allocated.");
        return EXIT_FAILURE;
  }

  while(ok == -1){

    //ar_ri
    ri(ar_ri, K, maxPoints, D);

    LSHclustering(ar_ri, LSH_Ar, curveAr, pinfo);

    int tableSize = numCurves/ 8;
    curvePtr* meanCenters = malloc(tableSize * sizeof(curvePtr));
    findBucketMean(LSH_Ar[0], curveAr, pinfo, meanCenters);

    for(i = 0; i < tableSize; i++){
      // printf("mean %d\n", meanCenters[i]->id);
    }


    //SILHOUETTE
    ok = silhouette(pinfo, curveAr, meanCenters, lsh_out, 2);

    // printf("with k %d silhouette returned %d\n", pinfo->K, ok);
    if(ok == -1){
      pinfo->K++;
      K = pinfo->K;
      ar_ri = realloc(ar_ri, sizeof(K*maxPoints*D * sizeof(int)));
      //initialize all
      for(i = 0; i < numCurves; i++){
        curveAr[i]->is_centroid = -1;
        curveAr[i]->has_centroid = -1;
      }
    }
  }

  BucketPtr* cur_HashTable = LSH_Ar[0];
  BucketPtr cur_bucket;
  for(i = 0; i < numCurves/8 ; i++){
    cur_bucket = cur_HashTable[i];
    if(cur_bucket->id != -1){
      fprintf(lsh_out,"%d  ", cur_bucket->id);
    }
    while(cur_bucket->next != NULL){
      cur_bucket = cur_bucket->next;
      if(cur_bucket->id != -1){
        fprintf(lsh_out,"%d  ", cur_bucket->id);
      }
    }
    fprintf(lsh_out,"\n");
  }
}


    //FREE

  for(c = 0; c < clusters; c++){
  if(centroidAr[c]->id >= numCurves){
  if(centroidAr[c]->cenBuckets != NULL)
  free(centroidAr[c]->cenBuckets);
  free(centroidAr[c]->coordinates);
  free(centroidAr[c]);
  // printf("FREE UPDATED CENTER\n" );
  }
  }
  free(centroidAr);

  for(i = 0; i < numCurves; i++){

  if(curveAr[i]->cenBuckets != NULL){
  free(curveAr[i]->cenBuckets);
  }

  free(curveAr[i]->coordinates);
  free(curveAr[i]->minus_centers);
  free(curveAr[i]);
  }
  free(curveAr);
  fclose(outf);
  fclose(lsh_out);
  free(pinfo);

  printf("Finished.\n" );
  return 0;
}
