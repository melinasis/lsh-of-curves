#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

struct string {
  char *ptr;
  size_t len;
};

void init_string(struct string *s) {
  s->len = 0;
  s->ptr = malloc(s->len+1);
  if (s->ptr == NULL) {
    fprintf(stderr, "malloc() failed\n");
    exit(EXIT_FAILURE);
  }
  s->ptr[0] = '\0';
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s)
{
  size_t new_len = s->len + size*nmemb;
  s->ptr = realloc(s->ptr, new_len+1);
  if (s->ptr == NULL) {
    fprintf(stderr, "realloc() failed\n");
    exit(EXIT_FAILURE);
  }
  memcpy(s->ptr+s->len, ptr, size*nmemb);
  s->ptr[new_len] = '\0';
  s->len = new_len;

  return size*nmemb;
}



int curlFunction(char* str, FILE* outf)
{
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();
  if(curl) {

    struct string s;
    init_string(&s);

    curl_easy_setopt(curl, CURLOPT_URL, str);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);

    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if(res != CURLE_OK){
      fprintf(stderr, "curl_easy_perform() failed: %s\n",curl_easy_strerror(res));
    }

    s.ptr[strlen(s.ptr) - 1] = '\0';
    fprintf(outf, "%s\t", s.ptr);
    free(s.ptr);
    /* always cleanup */
    curl_easy_cleanup(curl);
  }
  return 0;
}


int main()
{
    int count = 0;
    int flag = 0;
    char tag[2];
    memset(tag,'\0',sizeof(tag));

    FILE* stream = fopen("ways_ready", "r");
    FILE* outf = fopen("athens.csv", "w");
    FILE* type = fopen("type.csv", "w");

    long long int id;
    char* n;

    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, stream)) != -1) {
      if(flag == 1){
        flag = 0;
        continue;
      }
      line[read - 1] = 0;
      char *pch;

      pch = strtok(line, "=");
      strcpy(tag, pch);
      if(strcmp(tag, "id") == 0){
            pch = strtok(NULL, "=");
            id = strtoll(pch, NULL, 10);
            fprintf(outf, "%lld\t", id);
      }
      else if(strcmp(tag, "n") == 0){
        pch = strtok(NULL, "=");
        // printf("pch %s\n", pch);
        ///////////////////////////////////////////////////

        CURL *curl;
        CURLcode res;

        char *a = "http://overpass-api.de/api/interpreter?data=%5Bout:csv(::lat,::lon;false)%5D;(node(";
        char *b = "););out;";
        char str[1024];
        strcpy(str, a);
        strcat(str, pch);
        strcat(str, b);

        // printf("%s\n", str);
        curlFunction(str, outf);
        fflush(stdout);

      }
      else if(strcmp(tag, "v") == 0){
        pch = strtok(NULL, "=");
        fprintf(type, "%s\n", pch);
        fprintf(outf,"\n");
        flag = 1;
        count++;
        if(count == 1000){
          printf("1000 ok\n" );
          fflush(stdout);
          count = 0;
        }
      }
    }

    fclose(stream);
    fclose(outf);
    fclose(type);
    free(line);
}
