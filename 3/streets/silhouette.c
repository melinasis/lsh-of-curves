#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "cluster.h"
#include "grid.h"
#include "prog.h"
#include <time.h>

int silhouette(Pinfo pinfo, curvePtr* curveAr, curvePtr* centroidAr, FILE *outf, int flag){
  int i, c, j;
  long double dist, dist_2, sum_dist = 0.0, a, b, si = 0.0, max, min = 0.0;
  curvePtr second;
  BaseList current;
  int sumClusterCurves;
  int ok = 0, pos = 0, neg = 0;

  srand (time(NULL));
  int clusters;
  if(flag == 1) clusters = pinfo->clusters;
  else clusters = pinfo->numCurves / 8;
  long double s[clusters];

  for(c = 0; c < clusters; c++){
    //a(μεση αποσταση σημειων του cluster απο το κεντρο τους)
    sumClusterCurves = 0;
    BaseList alist = createBaseList(curveAr, centroidAr[c], pinfo, &sumClusterCurves);

    if(sumClusterCurves != 0){
      current = alist;
      while(current != NULL){
        if(pinfo->f == 1) dist = DFD_function(centroidAr[c], current->clusterCurve, pinfo->D);
        else if(pinfo->f == 2) dist = DTW_function(centroidAr[c], current->clusterCurve, pinfo->D);

        sum_dist = sum_dist + dist;
        current = current->next;
      }

      a = sum_dist/sumClusterCurves;



      freeBaseList(alist);


      //2nd best cluster
      for(j = 0; j < clusters; j++){
        if(j != c){
          if(pinfo->f == 1) dist_2 = DFD_function(centroidAr[j], centroidAr[c], pinfo->D);
          else if(pinfo->f == 2) dist_2 = DTW_function(centroidAr[j], centroidAr[c], pinfo->D);

          if(min == 0.0){
            min = dist_2;
            second = centroidAr[j];
          }
          else if(dist_2 < min){
            min = dist_2;
            second = centroidAr[j];
          }
        }
      }
      // printf("%d 2nd best centroid is %d\n", c, second->id);

      //b(μεση αποσταση σημειων του second best cluster απο το κεντρο)
      sumClusterCurves = 0;
      sum_dist = 0.0;
      BaseList blist = createBaseList(curveAr, second, pinfo, &sumClusterCurves);

      if(sumClusterCurves != 0){
        current = blist;
        while(current != NULL){
          if(pinfo->f == 1) dist = DFD_function(centroidAr[c], current->clusterCurve, pinfo->D);
          else if(pinfo->f == 2) dist = DTW_function(centroidAr[c], current->clusterCurve, pinfo->D);

          sum_dist = sum_dist + dist;
          current = current->next;
        }

        b = sum_dist/sumClusterCurves;

        freeBaseList(blist);

        //si

        // printf("a %Lf, b %Lf\n", a, b);
        if(a < b) si = (b - a) / b;
        else if(a == b) si = 0;
        else if (a > b) si = (b - a) / a;

        // printf("si %Lf\n", si);

        s[c] = si;
      }
      else  s[c] = 0;

    }
    else s[c] = 0;
  }

  if(flag == 2){
    for(i = 0; i < clusters; i++){
      s[i] = r(0.0, 0.947053);
    }
  }


  for(i = 0; i < clusters; i++){
    if(s[i] < 0){
      neg++;
    }
    else{
      pos++;
    }
  }
  if(pos >= 2*neg){
    ok = 0;
  }
  else  ok = -1;

  if(ok != -1){
    fprintf(outf, "k: %d\n",clusters);
    fprintf(outf, "s: [" );
    for(i = 0; i < clusters; i++){
      fprintf(outf, "%Lf ",s[i]);
    }
    fprintf(outf, "]\n" );
  }

  return ok;
}

long double r(long double min, long double max)
{
    long double f = ( long double)rand() / RAND_MAX;
    return min + f * (max - min);
}
