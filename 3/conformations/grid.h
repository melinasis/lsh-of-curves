#include <stdio.h>
#include <stdlib.h>

typedef struct Prog* Pinfo;
typedef struct Curve* curvePtr;

typedef struct Curve{
  int id;
  int points;
  int is_centroid;
  int has_centroid;
  int candidate;
  int* cenBuckets;
  long double* coordinates;
  double* minus_centers;
}Curve;

typedef struct node* nodePtr; 						//pointer to node

typedef struct node{
	long double point;
	struct node* next;
}node;

long double uniformlyRand(long double delta);
nodePtr grid(curvePtr curveP, Pinfo pinfo);
long double gridpoint(long double p, long double t, long double delta);
nodePtr createGridList();
void push(nodePtr head, long double x, long double y);
void concatenate(nodePtr head, nodePtr newList);
void printList(nodePtr head);
int countNode(nodePtr head);
int removeDuplicates(nodePtr head, long double x, long double y);
void freeGridList(nodePtr head);

nodePtr createHlist();
