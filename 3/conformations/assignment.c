#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cluster.h"
#include "prog.h"
#include "grid.h"
#include "hashTable.h"


//Lloyd's
void lloyds(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo){
  fflush(stdout);
  int D = pinfo->D;
  int k = pinfo->clusters;
  int numCurves = pinfo->numCurves;
  long double dist, min = 0.0;
  int i, j;


  for(i = 0; i < numCurves; i++){//for each curve
    if(curveAr[i]->is_centroid == -1){//non centroid
      for(j = 0; j < k; j++){//for each centroid
        //Distance
        dist = DFD_function(centroidAr[j], curveAr[i], pinfo->D);

        //keep min
        if(min == 0.0){
          min = dist;
          curveAr[i]->has_centroid = centroidAr[j]->id;
        }
        else if(dist < min){
          min = dist;
          curveAr[i]->has_centroid = centroidAr[j]->id;
        }
      }
    }
    //next curve
    min = 0.0;
  }
}
