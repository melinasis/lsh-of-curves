#define COUNT 5

typedef struct BucketList* BucketPtr;
typedef struct Prog* Pinfo;
typedef struct Curve* curvePtr;


//Binary Tree
typedef struct nodeTree* nodeTreePtr;
typedef struct nodeTree{
  curvePtr curve;
  struct nodeTree* left;
  struct nodeTree* right;
  int flag;
}nodeTree;

//Base List
typedef struct BaseListNode* BaseList;
typedef struct BaseListNode{
  curvePtr clusterCurve;
  BaseList next;
}BaseListNode;

//Objective Function
long double ObjectiveFunctionA(curvePtr* curveAr, curvePtr* candidates, Pinfo pinfo);
long double ObjectiveFunctionB(curvePtr centroid, curvePtr candidate, BaseList baselist, Pinfo pinfo);

//silhouette
int silhouette(Pinfo pinfo, curvePtr* curveAr, curvePtr* centroidAr, FILE *outf);

//update
BaseList createBaseList(curvePtr* curveAr, curvePtr centroid, Pinfo pinfo, int* sumClusterCurves);
void freeBaseList(BaseList BaseList);

nodeTreePtr createBinaryTree(int height, BaseList baseList);
void updateMDFC(int loop, curvePtr* curveAr, curvePtr centroid, Pinfo pinfo, int c, curvePtr updatedCenter);
int findHeight(int a);
void print2D(nodeTreePtr root);
void print2DUtil(nodeTreePtr root, int space);
curvePtr MeanDiscreteFrechetCurve(curvePtr curveA, curvePtr curveB, Pinfo pinfo, int c, nodeTreePtr node, int loop);
curvePtr PostOrderTraversal(nodeTreePtr node, Pinfo pinfo, int c, int loop);
void freeBinaryTree(nodeTreePtr node, int numCurves);


curvePtr updatePAM(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo);
curvePtr ClusterPAM(curvePtr* curveAr, curvePtr centroid, Pinfo pinfo, BaseList baseList, long double* new_objective);

//initialization
void randomCentroids(int* random_centroid_array, int numCurves, int centroids);
void randomInit(Pinfo pinfo, curvePtr* curveAr, curvePtr* centroidAr);

void kmeans(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo);


//assignment
void assignmentRangeSearch(int* ar_ri, int loop, BucketPtr** LSH_Ar, curvePtr* centroidAr,  curvePtr* curveAr, Pinfo pinfo, int** centroidL);
int rangeSearchBucket(BucketPtr* HashTable, int l, double R, Pinfo pinfo, curvePtr* curveAr, curvePtr* centroidAr,  int* currentCentroidAr);
long double DFD_function(curvePtr p,curvePtr q, int D);
long double DTW_function(curvePtr p,curvePtr q, int D);
long double max(long double d1,long double d2);
long double min(long double d1,long double d2,long double d3);
long double coor_dist(int D,int i,int j,long double *p,long double *q);
int mini_pos(long double a,long double b,long double c);
curvePtr AVG(int D,curvePtr p,curvePtr q, int numCurves, int c, int loop, int clusters);
long double dist(long double *x,long double *y, int D);
long double minDistCen(curvePtr* centroidAr, Pinfo pinfo);
void checkUnassigned(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo);


void lloyds(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo);
