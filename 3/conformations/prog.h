#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define KVEC 3

typedef struct Curve* curvePtr;
typedef struct Prog* Pinfo;
typedef struct Prog{
	int f;
	int D, K, L, clusters, numCurves, N;
	long double delta;
	char input[20], fd[3], output[20], conf[15];
}Prog;

int argHandling(Pinfo pinfo, int argc, char **argv);
void printHelp(char *progName);
void confHandling(Pinfo pinfo);
void firstRead(Pinfo pinfo, FILE *outf);
void secondRead(Pinfo pinfo, curvePtr* curveAr);
