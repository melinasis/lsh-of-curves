#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "prog.h"
#include "grid.h"

void printHelp(char *progName) {
    printf("Usage: %s arguments\n\
\033[22;33m-i\033[m [filename]  input file\n\
\033[22;33m-c\033[m [filename]  configuration file\n\
\033[22;33m-o\033[m  [filename] output file\n\
\033[22;33m-d\033[m  [DFD or CRMSD]\n", progName);

}

int argHandling(Pinfo pinfo, int argc, char **argv) {
    int argIter = 1, f = 0;
    char input[20], fd[5], output[20], conf[15];
    char* dfd, dtw;

    //initialize pinfo
    memset(pinfo->fd,'\0',sizeof(pinfo->fd));
    memset(pinfo->input,'\0',sizeof(pinfo->input));
    memset(pinfo->conf,'\0',sizeof(pinfo->conf));
    memset(pinfo->output,'\0',sizeof(pinfo->output));
    pinfo->f = 0;

    memset(fd,'\0',sizeof(fd));
    memset(input,'\0',sizeof(input));
    memset(conf,'\0',sizeof(conf));
    memset(output,'\0',sizeof(output));

    if(argc == 1) return EXIT_FAILURE;
    // printf("%d\n",argc );

    while (argIter < argc-1) {
        if (argv[argIter][0] == '-') {
            if (argv[argIter][2] == '\0') {
                switch (argv[argIter][1]) {
                  case 'i':
                       argIter++;
                       strcpy(input, argv[argIter]);
                      //  printf("hello %s\n",input);
                       break;
                  case 'c':
                       argIter++;
                       strcpy(conf, argv[argIter]);
                      //  printf("hello %s\n", conf);
                       break;
                 case 'o':
                      argIter++;
                      strcpy(output, argv[argIter]);
                      // printf("hello %s\n", output);
                      break;
                  case 'd':
                      argIter++;
                      strcpy(fd, argv[argIter]);
                      if(strcmp(fd, "DFD") == 0){
                          f = 1;
                      }
                      else if(strcmp(fd, "CRMSD") == 0){
                          f = 2;
                      }
                      // printf("hello %s\n", fd);
                      break;
                  default:
                      // printf("1\n" );
                      return EXIT_FAILURE;
              }
          }
          else{
            // printf("2\n" );
            return EXIT_FAILURE;
          }
      }
      else{
        // printf("3\n" );
        return EXIT_FAILURE;
      }
      ++argIter;
    }

    memcpy(pinfo->fd, fd, sizeof(fd));
    // printf("fd %s\n", pinfo->fd);

    pinfo->f = f;
    // printf("f %d\n", pinfo->f);

    memcpy(pinfo->input, input, sizeof(input));
    // printf("input %s\n", pinfo->input);

    memcpy(pinfo->output, output, sizeof(output));
    // printf("output %s\n", pinfo->output);

    memcpy(pinfo->conf, conf, sizeof(conf));
    // printf("conf %s\n", pinfo->conf);



    if(pinfo->f == 0 || strlen(pinfo->input) == 0 || strlen(pinfo->output) == 0 || strlen(pinfo->conf) == 0){
      // printf("f %d\n", pinfo->f);
      // printf("%s\n", pinfo->input);
      // printf("%s\n", pinfo->output);
      // printf("%s\n", pinfo->conf);
      // printf("4\n" );
      return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void confHandling(Pinfo pinfo){
  FILE *conf;
  conf = fopen(pinfo->conf,"r");    //configuration file

  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int temp_ar[3]={0,0,0};
  int ep=0;
  while((read = getline(&line, &len, conf)) != -1){
    line[read - 1] = 0;
    char *pch;
    pch = strtok(line, " ");
    pch = strtok(NULL, " ");
    if(pch == "-"){
      ep++;
      continue;
    }else{
      temp_ar[ep] = atoi(pch);
      ep++;
    }
  }
  if(temp_ar[0]>0){
    pinfo->clusters = temp_ar[0];
  }
  if(temp_ar[1]>0){
    pinfo->K = temp_ar[1];
  }
  else{
    pinfo->K = 2;
  }
  if(temp_ar[2]>0){
    pinfo->L = temp_ar[2];
  }
  else{
    pinfo->L = 3;
  }

  // printf("centroids: %d\n",pinfo->clusters);
  // printf("K %d\n",pinfo->K );
  // printf("L %d\n",pinfo->L );

  fclose(conf);
  free(line);
}

void firstRead(Pinfo pinfo, FILE *outf){
  int numCurves, points, D, N;
  int fl = 0;
  long double delta;

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  stream = fopen(pinfo->input, "r" );

  while ((read = getline(&line, &len, stream)) != -1) {
    line[read - 1] = 0;
    char *pch;

    if(fl == 0){  //first line: numConform
      pch = strtok(line, " ");
      numCurves = atoi(pch);
      D = 3;
      fl = 1;
      continue;
    }

    if(fl == 1){  //second line: N
      pch = strtok(line, " ");
      N = atoi(pch);
      fl = 2;

      delta = (4*D*N);

      pinfo->D = D;
      pinfo->delta = delta;
      pinfo->numCurves = numCurves;
      pinfo->N = N;

      // fprintf(outf,"Conformations are %d\n", pinfo->numCurves);
      // fprintf(outf,"DELTA = %Lf\n", pinfo->delta);

      break;
    }
  }
  fclose(stream);
  free(line);
}

void secondRead(Pinfo pinfo, curvePtr* curveAr){
  int id = 0, fl = 0, l = -1, position = 0;
  curvePtr curveP;

  int D = pinfo->D;

  FILE *stream;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  stream = fopen(pinfo->input, "r" );

  while ((read = getline(&line, &len, stream)) != -1) {
    line[read - 1] = 0;
    char *pch;

    if(fl == 0){  //first line: numConform
      fl = 1;
      continue;
    }

    if(fl == 1){  //second line: N
      fl = 2;
      continue;
    }

    //////////////////////////////////////////////////////////////////////

    if(l == -1){
      //create curve
      curveP = malloc(sizeof(Curve));
      if(curveP == NULL){
            printf("Error! memory not allocated.");
            return;
      }

      curveP->id = id;
      curveP->points = pinfo->N;
      curveP->is_centroid = -1;
      curveP->has_centroid = -1;
      curveP->cenBuckets = NULL;
      curveP->candidate = -1;

      curveP->coordinates = (long double*)malloc(D * curveP->points * sizeof(long double));
      if(curveP->coordinates == NULL) {
            printf("Error! memory not allocated.");
            return;
      }
      curveP->minus_centers = (double*)malloc(D * curveP->points * sizeof(double));
      if(curveP->minus_centers == NULL) {
            printf("Error! memory not allocated.");
            return;
      }
      l = 0;
    }

    //////////////////////////////////////////////////////////////////////
    //start reading coordinates
    pch = strtok(line, "\t");
    curveP->coordinates[l] = strtold(pch, NULL);

    // printf("%Lf\n", curveP->coordinates[l]);
    l++;

    pch = strtok(NULL, "\t");
    curveP->coordinates[l] = strtold(pch, NULL);
    // printf("%Lf\n", curveP->coordinates[l]);
    l++;

    pch = strtok(NULL, "\t");
    curveP->coordinates[l] = strtold(pch, NULL);
    // printf("%Lf\n", curveP->coordinates[l]);
    l++;

    if(l == ( pinfo->N * D )){
      //finished reading curve
      curveAr[position] = curveP;   //saved to array
      position++;
      id++;
      l = -1;
    }
  }

  fclose(stream);
  free(line);
}
