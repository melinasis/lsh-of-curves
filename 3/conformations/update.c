#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "cluster.h"
#include "grid.h"
#include "prog.h"

int globalVar = 0;

//Objective Function
long double ObjectiveFunctionA(curvePtr* curveAr, curvePtr* candidates, Pinfo pinfo){
  long double sum = 0.0;
  int i, c;
  for(c = 0; c < pinfo->clusters; c++){
    for(i = 0; i < pinfo->numCurves; i++){
      if(curveAr[i]->candidate == candidates[c]->id){
        sum = sum + DFD_function(curveAr[i], candidates[c], pinfo->D);
      }
    }
  }

  return sum;
}

long double ObjectiveFunctionB(curvePtr centroid, curvePtr candidate, BaseList baselist, Pinfo pinfo){
  long double sum = 0.0;
  int i, c;
  BaseList current = baselist;
  while(current != NULL){
    if(current->clusterCurve->id != candidate->id){
      sum = sum + DFD_function(candidate, current->clusterCurve, pinfo->D);
    }
    current = current->next;
  }
  if(centroid->id != candidate->id){
    sum = sum + DFD_function(candidate, centroid, pinfo->D);
  }
  return sum;
}

BaseList createBaseList(curvePtr* curveAr, curvePtr centroid, Pinfo pinfo, int* sumClusterCurves){
  int i, f = 1;
  int numCurves = pinfo->numCurves;

  BaseList head = NULL;
  BaseList new = NULL;

  for(i = 0; i < numCurves; i++){
    if(curveAr[i]->has_centroid == centroid->id){
      *sumClusterCurves = *sumClusterCurves + 1;

      new = malloc(sizeof(BaseListNode));

      new->clusterCurve = curveAr[i];
      new->next = head;

      head = new;
    }
  }
  return head;
}

void freeBaseList(BaseList baseList){
  BaseList temp;
  BaseList current = baseList;
  while(current != NULL){
    temp = current;
    current = current->next;
    free(temp);
  }
}

//b. PAM
curvePtr updatePAM(curvePtr* curveAr, curvePtr* centroidAr, Pinfo pinfo){
  int c, swap, i , j, d, changed, sumClusterCurves;
  BaseList baseList;
  long double min = 0.0, new_objective;
  curvePtr new, min_centroid;
  int clusters = pinfo->clusters;
  int numCurves = pinfo->numCurves;

  for(c = 0; c < clusters; c++){
    sumClusterCurves = 0;

    baseList = createBaseList(curveAr, centroidAr[c], pinfo, &sumClusterCurves);
    // printf("sum is %d\n",sumClusterCurves );

    if(baseList != NULL){
      //return best of cluster
      // printf("cluster %d\n",c );
      new_objective = 0.0;
      new = ClusterPAM(curveAr, centroidAr[c], pinfo, baseList, &new_objective);
      // printf("in cluster %d min is %d with value %Lf\n",c, new->id, new_objective );
      //find min of all clusters
      if(min == 0.0){
        min = new_objective;
        min_centroid = new;
      }
      else if(new_objective < min){
        min = new_objective;
        min_centroid = new;
      }
    }
    freeBaseList(baseList);
  }
  // printf("new is %d\n", min_centroid->id);
  // printf("from cluster with centroid %d\n", min_centroid->has_centroid);
  // printf("new objective %Lf\n",min );
  return min_centroid;
}

curvePtr ClusterPAM(curvePtr* curveAr, curvePtr centroid, Pinfo pinfo, BaseList baseList, long double* new_objective){
  curvePtr new;
  long double objective, min = 0.0;

  BaseList current = baseList;

  //Objective Function with current center
  min = ObjectiveFunctionB(centroid, centroid, baseList, pinfo);
  // printf("initial %Lf\n", min);
  new = centroid;

  while(current != NULL){
    objective = 0.0;
    //checking current point of cluster
    objective = ObjectiveFunctionB(centroid, current->clusterCurve, baseList, pinfo);
    // printf("new %Lf\n", objective);
    //find min of cluster
    if(objective < min){
      min = objective;
      new = current->clusterCurve;
    }
    current = current->next;
  }
  *new_objective = *new_objective + min;
  return new;
}
