#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "cluster.h"
#include "grid.h"
#include "prog.h"

int silhouette(Pinfo pinfo, curvePtr* curveAr, curvePtr* centroidAr, FILE *outf){
  int i, c, j;
  int ok = 0;
  long double dist, dist_2, sum_dist = 0.0, a, b, si = 0.0, max, min = 0.0;
  curvePtr second;
  BaseList current;
  int sumClusterCurves;
  int pos = 0; int neg = 0;

  int clusters = pinfo->clusters;
  long double s[clusters];

  for(c = 0; c < clusters; c++){
    //a(μεση αποσταση σημειων του cluster απο το κεντρο τους)
    sumClusterCurves = 0;
    BaseList alist = createBaseList(curveAr, centroidAr[c], pinfo, &sumClusterCurves);

    if(sumClusterCurves != 0){
      current = alist;
      while(current != NULL){
        dist = DFD_function(centroidAr[c], current->clusterCurve, pinfo->D);

        sum_dist = sum_dist + dist;
        current = current->next;
      }

      a = sum_dist/sumClusterCurves;


      freeBaseList(alist);


      //2nd best cluster
      for(j = 0; j < clusters; j++){
        if(j != c){
          dist_2 = DFD_function(centroidAr[j], centroidAr[c], pinfo->D);

          if(min == 0.0){
            min = dist_2;
            second = centroidAr[j];
          }
          else if(dist_2 < min){
            min = dist_2;
            second = centroidAr[j];
          }
        }
      }
      // printf("%d 2nd best centroid is %d\n", c, second->id);

      //b(μεση αποσταση σημειων του second best cluster απο το κεντρο)
      sumClusterCurves = 0;
      sum_dist = 0.0;
      BaseList blist = createBaseList(curveAr, second, pinfo, &sumClusterCurves);

      if(sumClusterCurves != 0){
        current = blist;
        while(current != NULL){
          dist = DFD_function(centroidAr[c], current->clusterCurve, pinfo->D);

          sum_dist = sum_dist + dist;
          current = current->next;
        }

        b = sum_dist/sumClusterCurves;

        freeBaseList(blist);

        //si
        // printf("a %Lf, b %Lf\n", a, b);
        if(a < b) si = (b - a) / b;
        else if(a == b) si = 0;
        else if (a > b) si = (b - a) / a;

        // printf("si %Lf\n", si);
        s[c] = si;
      }
      else  s[c] = 0;

    }
    else s[c] = 0;
  }

  for(i = 0; i < clusters; i++){
    if(s[i] < 0){
      neg++;
    }
    else{
      pos++;
    }
  }
  if(pos >= 2*neg){
    ok = 0;
  }
  else  ok = -1;

  if(ok != -1){
    fprintf(outf, "k: %d\n",clusters );
    fprintf(outf, "s: [" );
    for(i = 0; i < clusters; i++){
      fprintf(outf, "%Lf ",s[i]);
    }
    fprintf(outf, "]\n" );
  }

  return ok;
}
