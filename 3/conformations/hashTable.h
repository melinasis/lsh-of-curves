#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

typedef struct Neighbor* Neigh;
typedef struct Neighbor{
	int id;
	int n_id;
	long double dist;
}Neighbor;

typedef struct BucketList* BucketPtr;
typedef struct node* nodePtr; 						//pointer to node

typedef struct BucketList{
	int id;
	nodePtr g;
  BucketPtr next;
	int is_centroid;
  int has_centroid;
}BucketList;

BucketPtr* createHashTable(int numCurves);
void insertHashTable(BucketPtr* HashTable, int key, curvePtr curveP,  nodePtr g);
void printHashTable(BucketPtr* HashTable, int numCurves);
void freeHashTable(BucketPtr* HashTable, int numCurves, int l);
void freeBucketList(BucketPtr head, int l);

//LSH
void LSH_Hash(nodePtr g, int numCurves, BucketPtr* LSH_HashTable, curvePtr curveP, int ar_ri[], int D, int l);
double t_calc(int w);
void push_h(nodePtr head, long double p);
void concatenate_lsh(nodePtr head, nodePtr newList);
int LSH_keyValue(nodePtr g, int numCurves, int ar_ri[], int D);
void free_amplified(nodePtr head);
void marsaglia(long double* v, int D);
void moveCentroidsStart(BucketPtr* HashTable, Pinfo pinfo, curvePtr* centroidAr, int l);
void resetHashTable(BucketPtr* HashTable, int numCurves);
void centroid_flag(BucketPtr* HashTable, int numCurves, curvePtr curveP, int l);
void ri(int* ar_ri, int K, int maxPoints, int D);
int findBucket(BucketPtr* HashTable, int id, int numCurves);
