Ανάπτυξη Λογισμικού για Αλγοριθμικά Προβλήματα 2017
Μέρος Γ1

Μελίνα Σίσκου 1115201200161
Γεώργιος Ρούσσος - Μπάλλας 1115201300151

Για compile εντολή make.
Γραμμή εντολής ./proj -i inputfile -d [DFD or DTW] -o outputfile -c conf

Conf file:	
number_of_clusters: 
number_of_grid_curves: - (default)
number_of_hash_tables: 1

Κατάλογος αρχείων:
main.c			prog.h
prog.c			grid.h
grid.c			hashTable.h
classicHash.c		cluster.h
LSH.c			conformations.h	
initialization.c	Makefile
assignment.c
update.c
distances.c
silhouette.c
segment.c
parsing.c

OSM parsing: 
Χρησιμοποιώντας το osmfilter κρατάμε μόνο τα ways με tag=highway και 
value=service,  motorway, trunk, primary, secondary, tertiary, unclassified, residential.

Έπειτα υλοποιήσαμε το πρόγραμμα parsing.c, το οποίο για κάθε way καλεί την εντολή curl έτσι ώστε να επιστραφούν οι συντεταγμένες των κόμβων του.
Αυτό επιτυγχάνεται μέσω του Overpass api. Τα αποτελέσματα γράφονται στο αρχείο athens.csv. 

Google Drive link: https://drive.google.com/file/d/1falJ5XsGiPsCrUi8DbnmvGgaKtvgiwQ6/view
Για γρήγορη εκτέλεση χρησιμοποιείται το athens100.csv.

Segmentation:
Υλοποιήθηκε το αρχείο segment.c 
Αποθηκεύονται όλες οι καμπύλες στον πίνακα curveAr. Κάθε κόμβος εξετάζεται ως προς την καμπυλότητά του ή ως προς την ύπαρξη του και σε άλλη καμπύλη(διασταύρωση). Σε αυτές τις περιπτώσεις κόβεται, αφού πρώτα βεβαιωθούμε πως το τμήμα δεν είναι πολύ μικρό(λιγότεροι από 3 κόμβοι). Αν το τμήμα μεγαλώνει (περισσότεροι από 15 κόμβοι) χωρίς να έχει ξεπεραστεί το κατώφλι της καμπυλότητας ή δεν έχει βρεθεί διασταύρωση, η καμπύλη κόβεται.


ΙINITIALIZATION: Kmeans
ASSIGNMENT: Lloyd's
UPDATE: PAM

CRMSD: Για δρόμους διαφορετικών διαστάσεων χρησιμοποιείται το ελάχιστο των σημείων μεταξύ των δύο.

Clustering με LSH:
Αν έχει επιλεχθεί από την γραμμή εντολής η συνάρτηση απόστασης DFD τότε υλοποιείται και clustering χρησιμοποιώντας ως clusters τα buckets του
πίνακα κατακερματισμού. Για την αξιολόγηση των κέντρων μέσω της σιλουέττας υπολογίζεται το mean κέντρο με την μέθοδο του δυαδικού δέντρου και υλοποιείται η ίδια επαναληψη με αυτή στις πρωτεΐνες , μεταβάλλοντας τις καμπύλες πλέγματος του lsh, μέχρι να κριθούν τα κέντρα ικανοποιητικά.
